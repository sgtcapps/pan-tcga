# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141220191833) do

  create_table "cancer_types", force: true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cancer_types", ["abbreviation"], name: "index_cancer_types_on_abbreviation", unique: true, using: :btree
  add_index "cancer_types", ["name"], name: "index_cancer_types_on_name", unique: true, using: :btree

  create_table "clin_par_val_targ_sets", force: true do |t|
    t.integer  "clinical_parameter_value_id"
    t.integer  "target_set_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "clin_par_val_targ_sets", ["clinical_parameter_value_id"], name: "index_clin_par_val_targ_sets_on_clinical_parameter_value_id", using: :btree

  create_table "clinical_parameter_sub_cats", force: true do |t|
    t.string   "name"
    t.integer  "clinical_parameter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clinical_parameter_values", force: true do |t|
    t.string   "clin_param_value"
    t.integer  "sample_id"
    t.integer  "clinical_parameter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "data_set_id"
    t.integer  "cancer_type_id"
    t.integer  "target_genes_flag"
    t.integer  "all_mir_flag"
    t.integer  "all_protein_flag"
  end

  add_index "clinical_parameter_values", ["all_mir_flag"], name: "index_clinical_parameter_values_on_all_mir_flag", using: :btree
  add_index "clinical_parameter_values", ["all_protein_flag"], name: "index_clinical_parameter_values_on_all_protein_flag", using: :btree
  add_index "clinical_parameter_values", ["cancer_type_id"], name: "index_clinical_parameter_values_on_cancer_type_id", using: :btree
  add_index "clinical_parameter_values", ["clinical_parameter_id"], name: "index_clinical_parameter_values_on_clinical_parameter_id", using: :btree
  add_index "clinical_parameter_values", ["data_set_id"], name: "index_clinical_parameter_values_on_data_set_id", using: :btree
  add_index "clinical_parameter_values", ["sample_id"], name: "index_clinical_parameter_values_on_sample_id", using: :btree
  add_index "clinical_parameter_values", ["target_genes_flag"], name: "index_clinical_parameter_values_on_target_genes_flag", using: :btree

  create_table "clinical_parameters", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "data_sets", force: true do |t|
    t.date     "analysis_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "gene_cn_frequencies", force: true do |t|
    t.integer  "gene_id"
    t.integer  "total_samples"
    t.decimal  "total_percent",  precision: 8, scale: 4
    t.decimal  "homozygous_del", precision: 8, scale: 4
    t.decimal  "hemizygous_del", precision: 8, scale: 4
    t.decimal  "no_change",      precision: 8, scale: 4
    t.decimal  "gain",           precision: 8, scale: 4
    t.decimal  "amplification",  precision: 8, scale: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "target_set_id"
  end

  create_table "gene_mut_frequencies", force: true do |t|
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "target_set_id"
    t.integer  "gene_id"
    t.integer  "total_samples"
    t.integer  "num_mutated_samples"
    t.decimal  "frequency",           precision: 8, scale: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "genes", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

  add_index "genes", ["name"], name: "index_genes_on_name", using: :btree

  create_table "genomic_values", force: true do |t|
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "genomic_id"
    t.integer  "sample_id"
    t.integer  "target_id"
    t.string   "target_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "value"
  end

  add_index "genomic_values", ["sample_id"], name: "index_genomic_values_on_sample_id", using: :btree

  create_table "genomics", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "high_level_genomics", force: true do |t|
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "high_level_summaries", force: true do |t|
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "target_set_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "clinical_parameter_id"
    t.integer  "clinical_parameter_sub_cat_id"
    t.integer  "high_level_genomic_id"
  end

  add_index "high_level_summaries", ["cancer_type_id"], name: "cancer_type_id", using: :btree

  create_table "high_level_summary_values", force: true do |t|
    t.integer  "high_level_summary_id"
    t.integer  "genomic_id"
    t.integer  "num_samples"
    t.string   "num_genomics"
    t.string   "num_rank_target_sets"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mirs", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

  add_index "mirs", ["name"], name: "index_mirs_on_name", using: :btree

  create_table "outcome_summaries", force: true do |t|
    t.integer  "rank"
    t.decimal  "total_score",                   precision: 8, scale: 2
    t.integer  "num_selected_genomic_features"
    t.string   "copy_number"
    t.string   "m_rna_expression"
    t.string   "methylation"
    t.text     "source_types"
    t.integer  "num_source_types"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "target_set_id"
    t.integer  "high_level_genomic_id"
    t.integer  "genomic_id"
    t.integer  "clinical_parameter_id"
    t.integer  "clinical_parameter_sub_cat_id"
    t.string   "mi_rna_expression"
    t.string   "mutation"
    t.string   "genomic_features"
    t.string   "protein_expression"
    t.string   "target_type"
    t.integer  "target_id"
  end

  create_table "proteins", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

  add_index "proteins", ["name"], name: "index_proteins_on_name", using: :btree

  create_table "samples", force: true do |t|
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sample_code"
  end

  add_index "samples", ["sample_code"], name: "index_samples_on_sample_code", using: :btree

  create_table "target_gene_values", force: true do |t|
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "gene_id"
    t.integer  "genomic_id"
    t.integer  "sample_id"
    t.string   "gene_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "target_gene_values", ["cancer_type_id"], name: "index_target_gene_values_on_cancer_type_id", using: :btree
  add_index "target_gene_values", ["data_set_id"], name: "index_target_gene_values_on_data_set_id", using: :btree
  add_index "target_gene_values", ["gene_id"], name: "index_target_gene_values_on_gene_id", using: :btree
  add_index "target_gene_values", ["sample_id"], name: "index_target_gene_values_on_sample_id", using: :btree

  create_table "target_mir_values", force: true do |t|
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "mir_id"
    t.integer  "genomic_id"
    t.integer  "sample_id"
    t.string   "mir_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "target_mir_values", ["cancer_type_id"], name: "index_target_mir_values_on_cancer_type_id", using: :btree
  add_index "target_mir_values", ["data_set_id"], name: "index_target_mir_values_on_data_set_id", using: :btree
  add_index "target_mir_values", ["mir_id"], name: "index_target_mir_values_on_mir_id", using: :btree
  add_index "target_mir_values", ["sample_id"], name: "index_target_mir_values_on_sample_id", using: :btree

  create_table "target_protein_values", force: true do |t|
    t.integer  "cancer_type_id"
    t.integer  "data_set_id"
    t.integer  "protein_id"
    t.integer  "genomic_id"
    t.integer  "sample_id"
    t.string   "protein_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "target_protein_values", ["cancer_type_id"], name: "index_target_protein_values_on_cancer_type_id", using: :btree
  add_index "target_protein_values", ["data_set_id"], name: "index_target_protein_values_on_data_set_id", using: :btree
  add_index "target_protein_values", ["protein_id"], name: "index_target_protein_values_on_protein_id", using: :btree
  add_index "target_protein_values", ["sample_id"], name: "index_target_protein_values_on_sample_id", using: :btree

  create_table "target_sets", force: true do |t|
    t.string   "name"
    t.integer  "cancer_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
