PanTcga::Application.routes.draw do
  
  root  'welcome#index', via: [:get, :post]
  
  # Static Pages
  match 'about', to: 'static_pages#about', via: 'get'
  match 'faq', to: 'static_pages#faq', via: 'get'
  #match 'contact', to: 'static_pages#contact', via: 'get'
  match 'project_overview', to: 'static_pages#project_overview', via: 'get'
  match 'analysis_pipeline', to: 'static_pages#analysis_pipeline', via: 'get'
  match 'target_selection', to: 'static_pages#target_selection', via: 'get'
  match 'data_information', to: 'static_pages#data_information', via: 'get'
  match 'data_download', to: 'static_pages#data_download', via: 'get'
  match 'show_target_selection_file' => 'static_pages#show_target_selection_file', via: 'get'
  match 'target_selection_send_data' => 'static_pages#target_selection_send_data', via: 'get'
  match 'tutorial' => 'static_pages#tutorial_overview', via: 'get'
  match 'ppt_sendfile' => 'static_pages#ppt_sendfile', via: 'get'
  match 'send_zip_file' => 'static_pages#send_zip_file', via: [:get, :post]
  
  # Cancer Types
  match '/get_cancer_type', to: 'cancer_types#get_cancer_type', via: 'post'

  # High Level Summaries
  match 'high_level_summary', to: 'high_level_summaries#index', via: [:get, :post]
  match 'show_high_level_summary_panels', to: 'high_level_summaries#show_high_level_summary_panels', via: [:get, :post]
  match 'high_level_summary_table', to: 'high_level_summaries#show_table', via: 'post'
  match 'high_level_data_analysis', to: 'high_level_summaries#high_level_data_analysis', via: [:get, :post]
  match 'query_clin_param_vals', to: 'high_level_summaries#query_clin_param_vals', via: [:get, :post]

  # Outcome Summaries
  match 'outcome_summary', to: 'outcome_summaries#index', via: [:get, :post]
  match 'search_outcome_tables', to: 'outcome_summaries#search_outcome_tables', via: [:get, :post]
  match 'send_file' => 'outcome_summaries#send_file', via: [:get, :post]

  # Matrix
  match 'matrix_query', to: 'matrix#index', via: [:get, :post]
  match 'target_genomic_query', to: 'matrix#target_genomic_by_target_genomic', via: [:get, :post] 
  match 'get_matrix_data', to: 'matrix#get_matrix_data', via: [:get, :post] 
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
