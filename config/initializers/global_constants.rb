GIT_BRANCH = `git status | sed -n 1p`.split(" ").last
GIT_COMMIT = `git log | sed -n 1p`.split(" ").last

# genomics with continuous, float values: rna-Seq, rna-Array, Protein-exp, mir-Seq, all methylations
#BOXPLOT_GENOMIC_IDS = [14,15,10,9,12,13,5]
BOXPLOT_GENOMIC_IDS = [1,2,3,6]
# genomics with discrete values: CN-Gistic2thres, MAF  
#HISTOGRAM_GENOMIC_IDS = [8,11]
HISTOGRAM_GENOMIC_IDS = [4,5]

STORED_SAMPLE_ID_PATH = "#{Rails.root}/sample_ids/"
