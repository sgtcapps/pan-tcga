# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $("#query_for_matrix_target_set_id3").click ->
    $("#query_for_matrix_target_name").val("")

$ ->
  if $("#chart_rows").length
    num_rows = $("#chart_rows").data("rowcount")
    #console.log "row count: " + num_rows
    for idx in [0...num_rows] by 1
      if $("#box_plot_container_" + idx).length
        genomic_id = $("#box_plot_container_" + idx).data("genomic-id") 
        chart_data = $("#box_plot_container_" + idx).data("chart-data") # array of arrays
        categories = $("#box_plot_container_" + idx).data("categories")
        readable_categories = $("#box_plot_container_" + idx).data("readable-categories")
        query_type = $("#box_plot_container_" + idx).data("query-type")
        this_title = $("#box_plot_container_" + idx).data("title")
        #console.log "type: " + "Box Plot"
        #console.log "idx: " + idx
        #console.log "genomic_id: " + genomic_id
        #console.log "chart data: " + chart_data
        #console.log "categories: " + categories
        #console.log "readable_categories: " + readable_categories
        if query_type == '2point'
          xaxis_label = 'Genomic Categories'
          categories = readable_categories
        else
          xaxis_label = 'Clinical Parameters' 
        $("#box_plot_container_" + idx).highcharts
          chart:
            type: "boxplot"

          title: 
            text: this_title

          credits: false

          legend:
            enabled: false

          xAxis:
            categories: categories
            title: 
              text: xaxis_label

          yAxis:
            title:
              text: "Genomic Values"

          series: [
            { 
              name: "Values"
              data: chart_data
              tooltip:
                headerFormat: "<strong>Category: {point.key}</strong><br>"
            }
  #           {
  #            name: "Outlier"
  #            color: Highcharts.getOptions().colors[0]
  #            type: "scatter"
  #            data: [ # x, y positions where 0 is the first category
  #               [
  #                 0
  #                 16.3054561660525
  #               ]
  #             ]
  #             marker:
  #               fillColor: "white"
  #               lineWidth: 1
  #               lineColor: Highcharts.getOptions().colors[0]

  #             tooltip:
  #               pointFormat: "Point: {point.y}"
  #           }
          ]
      else if $("#histogram_container_" + idx).length
        genomic_id = $("#histogram_container_" + idx).data("genomic-id") 
        categories = $("#histogram_container_" + idx).data("categories")
        readable_categories = $("#histogram_container_" + idx).data("readable-categories") 
        chart_data = $("#histogram_container_" + idx).data("chart-data") # array of arrays
        genomic_val_type_count_arr = chart_data

        #console.log "type: " + "Histogram"
        #console.log "idx: " + idx
        #console.log "genomic_id: " + genomic_id
        #console.log "chart data: " + chart_data
        #console.log "categories: " + categories
        #console.log "readable_categories: " + readable_categories
        #console.log "genomic_val_type_count_arr: " + genomic_val_type_count_arr
        #console.log "genomic_val_type_count_arr len: " + genomic_val_type_count_arr.length
        #console.log "categories len: " + categories.length

        series = []
        i = 0
        data_arr = []
        while i < readable_categories.length
          #console.log "name: " + readable_categories[i]
          $.each genomic_val_type_count_arr, (index, value) -> # same length as categories
            #console.log "genomic_val_type_count_arr index|value: " + index + "|" + value
            #console.log "genomic_val_type_count_arr[index][i]: " + genomic_val_type_count_arr[index][i]
            data_arr.push(genomic_val_type_count_arr[index][i]) # rearrange data by array item, column
          #console.log "data_arr: " + data_arr                    
          series.push
            name: readable_categories[i]
            data: data_arr
          data_arr = []
          i++
        #console.log "series: " + series

        $("#histogram_container_" + idx).highcharts
          chart:
            type: "column"

          title: false

          xAxis:
            categories: categories
            title: 
              text: "Clinical Parameters"

          yAxis:
            min: -10
            labels: 
              format: '{value}'
            title:
              text: "Number of Patients"

          credits:
            enabled: false

          series: series

      else if $("#stacked_bar_container_" + idx).length
        genomic_id = $("#stacked_bar_container_" + idx).data("genomic-id") 
        categories = $("#stacked_bar_container_" + idx).data("categories")
        readable_categories = $("#stacked_bar_container_" + idx).data("readable-categories") 
        chart_data = $("#stacked_bar_container_" + idx).data("chart-data") # array of arrays
        genomic_val_type_count_arr = chart_data

        #console.log "type: " + "Stacked Bar"
        #console.log "idx: " + idx
        #console.log "genomic_id: " + genomic_id
        #console.log "chart data: " + chart_data
        #console.log "categories: " + categories
        #console.log "readable_categories: " + readable_categories
        #console.log "genomic_val_type_count_arr: " + genomic_val_type_count_arr
        #console.log "genomic_val_type_count_arr len: " + genomic_val_type_count_arr.length
        #console.log "categories len: " + categories.length

        series = []
        i = 0
        data_arr = []
        while i < readable_categories.length
          #console.log "name: " + readable_categories[i]
          $.each genomic_val_type_count_arr, (index, value) -> # same length as categories
            data_arr.push(genomic_val_type_count_arr[index][i]) # rearrange data by array item, column
          #console.log "data_arr: " + data_arr                    
          series.push
            name: readable_categories[i]
            data: data_arr
          data_arr = []
          i++
        #console.log "series: " + series

        $("#stacked_bar_container_" + idx).highcharts
          chart:
            type: "bar"

          title: false

          xAxis:
            categories: categories
            title: 
              text: "Clinical Parameters"
            
          yAxis:
            min: 0
            title: 
              text: "Number of Patients"

          legend:
            reversed: true

          plotOptions:
            series:
              stacking: "normal"

          credits:
            enabled: false

          series: series
  return
  
$ ->
  if $("#genomic_by_genomic_barchart_container").length
   this_title = $("#genomic_by_genomic_barchart_container").data("title")
  $("#genomic_by_genomic_barchart_container").highcharts  
    
    data:
      table: document.getElementById("genomic_by_genomic_table")

    chart:
      type: "bar"

    title: 
      text: this_title

    yAxis:
      min: 0
      allowDecimals: true
      title:
        text: "#Samples"
    
    legend:
      reversed: true
      
    plotOptions:
      series:
        stacking: "normal"
        
    credits:
      enabled: false

    tooltip:
      formatter: ->
        "<b>" + @series.name + "</b><br/>" + "<b>" + @point.name + ": " + @point.y + "</b>"

  return

# render data analysis in matrix tab panels
# for dynamically rendered tabs
$ ->
  if $("#matrix_analysis_tabs").length
    g_ids = $("#matrix_analysis_tabs").data("genomic-ids")
    $.each g_ids, (index, g_id) ->
      $("#matrix_tab_" + g_id).click ->
        if ($("#matrix_tab_" + g_id).attr('disabled') == undefined)
          genomic_id = $("#matrix_tab_" + g_id).data("genomic-id")
          target_name = $("#matrix_tab_" + g_id).data("target-name")
          chart_title = $("#matrix_tab_" + g_id).data("chart-title")
          $("#matrix_tab_" + g_id).attr('disabled', true) 
          $.ajax
            url: "get_matrix_data"
            type: "POST"
            dataType: "script"
            data:
              genomic_id: genomic_id,
              target_name: target_name,
              chart_title: chart_title

          error: (jqXHR, textStatus, errorThrown) ->
            console.log textStatus

          success: (data, textStatus, jqXHR) ->
            console.log textStatus
        else
          return 
      return

# for default tab
$ ->
  if $("#matrix_tab_default").length
    genomic_id = $("#matrix_tab_default").data("genomic-id")
    target_name = $("#matrix_tab_default").data("target-name")
    chart_title = $("#matrix_tab_default").data("chart-title")
    $.ajax
      url: "get_matrix_data"
      type: "POST"
      dataType: "script"
      data:
        genomic_id: genomic_id,
        target_name: target_name,
        chart_title: chart_title

      error: (jqXHR, textStatus, errorThrown) ->
        console.log textStatus

      success: (data, textStatus, jqXHR) ->
        console.log textStatus 
  return

