# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $("#myCarousel.slide").carousel
    interval: 0
    pause: "hover"
    
#############################################
# replace placeholder in target name fields #
# and select options in select box          #
#############################################

# select genomic value arrays
gene1 = [
  {
    display: "Select Gene Genomic"
    value: ""
  }
  {
    display: "Copy Number"
    value: "4"
  }
  {
    display: "Mutation"
    value: "5"
  }
  {
    display: "RNA Seq"
    value: "6"
  }
]

gene2 = [
  {
    display: "Select Gene Genomic"
    value: ""
  }
  {
    display: "Copy Number"
    value: "4"
  }
  {
    display: "Mutation"
    value: "5"
  }
]

protein = [
  {
    display: "Protein Expression"
    value: "2"
  }
]

mirna1 = [
  {
    display: "Select miRNA Genomic"
    value: ""
  }
  {
    display: "Copy Number"
    value: "4"
  }
  {
    display: "miRNA Seq"
    value: "3"
  }
]

mirna2 = [
  {
    display: "Copy Number"
    value: "4"
  }
]
 

# function to populate select box 1 for 2 point query
select_list1 = (array_list) ->
  $("#query_for_target_genomic_genomic_id").html "" #reset child options
  $(array_list).each (i) -> #populate child options
    $("#query_for_target_genomic_genomic_id").append "<option value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>"
    return
  return

select_list2 = (array_list) ->
  $("#query_for_target_genomic_genomic_id2").html "" #reset child options
  $(array_list).each (i) -> #populate child options
    $("#query_for_target_genomic_genomic_id2").append "<option value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>"
    return
  return

# query_for_outcome_summary
$ ->
  $("#query_for_outcome_summary_target_set_id1").click ->
    $(".typeahead_gene_outcome").val("")
    $(".typeahead_gene_outcome").show()
    $(".typeahead_protein_outcome").hide()
    $(".typeahead_mir_outcome").hide()

$ ->
  $("#query_for_outcome_summary_target_set_id2").click ->
    $(".typeahead_protein_outcome").val("")
    $(".typeahead_protein_outcome").show()
    $(".typeahead_gene_outcome").hide()    
    $(".typeahead_mir_outcome").hide()

$ ->
  $("#query_for_outcome_summary_target_set_id3").click ->
    $(".typeahead_mir_outcome").val("")
    $(".typeahead_mir_outcome").show()
    $(".typeahead_gene_outcome").hide()
    $(".typeahead_protein_outcome").hide()


# query_for_matrix
$ ->
  $("#query_for_matrix_target_set_id1").click ->
    $(".typeahead_gene_matrix").val("")
    $(".typeahead_gene_matrix").show()
    $(".typeahead_protein_matrix").hide()
    $(".typeahead_mir_matrix").hide()

$ ->
  $("#query_for_matrix_target_set_id2").click ->
    $(".typeahead_protein_matrix").val("")
    $(".typeahead_gene_matrix").hide()
    $(".typeahead_protein_matrix").show()
    $(".typeahead_mir_matrix").hide()

$ ->
  $("#query_for_matrix_target_set_id3").click ->
    $(".typeahead_mir_matrix").val("")
    $(".typeahead_gene_matrix").hide()
    $(".typeahead_protein_matrix").hide()
    $(".typeahead_mir_matrix").show()

# query_for_target_genomic first pair 
# gene
$ ->
  if $("#query_for_target_genomic_target_set_id1a").is(":checked")
    select_list1 gene1
    $(".typeahead_gene_targ_genomic1").val("")
    $(".typeahead_gene_targ_genomic1").show()
    $(".typeahead_protein_targ_genomic1").hide()
    $(".typeahead_mir_targ_genomic1").hide()

$ ->
  $("#query_for_target_genomic_target_set_id1a").click ->
    select_list1 gene1
    $(".typeahead_gene_targ_genomic1").val("")
    $(".typeahead_gene_targ_genomic1").show()
    $(".typeahead_protein_targ_genomic1").hide()
    $(".typeahead_mir_targ_genomic1").hide()

# protein
$ ->
  if $("#query_for_target_genomic_target_set_id2a").is(":checked")
    select_list1 protein
    $(".typeahead_protein_targ_genomic1").val("")
    $(".typeahead_gene_targ_genomic1").hide()
    $(".typeahead_protein_targ_genomic1").show()
    $(".typeahead_mir_targ_genomic1").hide()

$ ->
  $("#query_for_target_genomic_target_set_id2a").click ->
    select_list1 protein
    $(".typeahead_protein_targ_genomic1").val("")
    $(".typeahead_gene_targ_genomic1").hide()
    $(".typeahead_protein_targ_genomic1").show()
    $(".typeahead_mir_targ_genomic1").hide()

# mirna
$ ->
  if $("#query_for_target_genomic_target_set_id3a").is(":checked")
    select_list1 mirna1
    $(".typeahead_mir_targ_genomic1").val("")
    $(".typeahead_gene_targ_genomic1").hide()
    $(".typeahead_protein_targ_genomic1").hide()
    $(".typeahead_mir_targ_genomic1").show()

$ ->
  $("#query_for_target_genomic_target_set_id3a").click ->
    select_list1 mirna1
    $(".typeahead_mir_targ_genomic1").val("")
    $(".typeahead_gene_targ_genomic1").hide()
    $(".typeahead_protein_targ_genomic1").hide()
    $(".typeahead_mir_targ_genomic1").show()
    
# query_for_target_genomic 2nd pair
# gene
$ ->
  if $("#query_for_target_genomic_target_set_id1b").is(":checked")  
    select_list2 gene2
    $(".typeahead_gene_targ_genomic2").val("")
    $(".typeahead_gene_targ_genomic2").show()
    $(".typeahead_protein_targ_genomic2").hide()
    $(".typeahead_mir_targ_genomic2").hide()
 
$ ->
  $("#query_for_target_genomic_target_set_id1b").click ->
    select_list2 gene2
    $(".typeahead_gene_targ_genomic2").val("")
    $(".typeahead_gene_targ_genomic2").show()
    $(".typeahead_protein_targ_genomic2").hide()
    $(".typeahead_mir_targ_genomic2").hide()

# mirna
$ ->
  if $("#query_for_target_genomic_target_set_id3b").is(":checked")
    select_list2 mirna2
    $(".typeahead_mir_targ_genomic2").val("")
    $(".typeahead_gene_targ_genomic2").hide()
    $(".typeahead_mir_targ_genomic2").show()

$ ->
  $("#query_for_target_genomic_target_set_id3b").click ->
    select_list2 mirna2
    $(".typeahead_mir_targ_genomic2").val("")
    $(".typeahead_mir_targ_genomic2").show()
    $(".typeahead_gene_targ_genomic2").hide()
    
    
###############
# validations #
###############
$ ->
  $("#choose_cancer_type").submit ->    
    if $("#cancer_type_id").val() is ""
       $("#error_ct").show().html('<div class="alert alert-danger query-error">Cancer Type selection is required</div>')
       false

$ ->
  $("#choose_clinical_parameter").submit ->    
    if $("#clinical_parameter_id").val() is ""
       $("#error_cp").show().html('<div class="alert alert-danger query-error">Clinical Parameter selection is required</div>')
       false
       
# validate query_for_matrix
$ ->
  $("#query_for_matrix").submit ->
    if $("#query_for_matrix_cancer_type_id").val() is ""
       $("#error_query_for_matrix").show().html('<div class="alert alert-danger query-error">Cancer Type selection is required</div>')
       false
    else if $("#query_for_matrix_clinical_parameter_id").val() is ""
       $("#error_query_for_matrix").show().html('<div class="alert alert-danger query-error">Clinical Parameter selection is required</div>')
       false
       
# validate target_genomic_query
$ ->
  $("#query_for_target_genomic").submit ->
    radios1 = $('input[id_name=query_for_target_genomic_target_set1_id]:checked')
    radios2 = $('input[id_name=query_for_target_genomic_target_set2_id]:checked')
    error = 'false'
    if $("#query_for_target_genomic_cancer_type_id").val() is ""
      $("#error_query_for_target_genomics").show().html('<div class="alert alert-danger query-error">Cancer Type selection is required</div>')
      error = 'true'
      false
    else if $("#query_for_target_genomic_genomic_id").val() is ""
      $("#error_query_for_target_genomics").show().html('<div class="alert alert-danger query-error">First Genomic selection is required</div>')
      error = 'true'
      false  
    else if $("#query_for_target_genomic_genomic_id2").val() is ""
      $("#error_query_for_target_genomics").show().html('<div class="alert alert-danger query-error">Second Genomic selection is required</div>')
      error = 'true'
      false
    else if error == 'false'
      $("#loading_icon_for_target_genomics").show()
      $("#error_query_for_target_genomics").hide()   
      return

######################
# Typeahead autofill #
######################
$ ->
  substringMatcher = (strs) ->
    findMatches = (q, cb) ->
      matches = undefined
      substrRegex = undefined
      
      # an array that will be populated with substring matches
      matches = []
      
      # regex used to determine if a string contains the substring `q`
      #substrRegex = new RegExp(q, "i")
      # regex used to determine if a string begins with the substring `q`
      substrRegex = new RegExp("^".concat(q), "i")
      
      # iterate through the pool of strings and for any string that
      # contains the substring `q`, add it to the `matches` array
      $.each strs, (i, str) ->
        
        # the typeahead jQuery plugin expects suggestions to a
        # JavaScript object, refer to typeahead docs for more info
        matches.push value: str  if substrRegex.test(str)
        return

      cb matches
      return

  genelist = undefined
  proteinlist = undefined
  mirlist = undefined
  genelist = $("#get_genes_list").data("targetgenes")
  proteinlist = $("#get_proteins_list").data("targetproteins")
  mirlist = $("#get_mirs_list").data("targetmirs")

# outcome summaries query
  $(".typeahead_gene_outcome").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "genelist"
    displayKey: "value"
    source: substringMatcher(genelist)

  $(".typeahead_protein_outcome").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "proteinlist"
    displayKey: "value"
    source: substringMatcher(proteinlist)

  $(".typeahead_mir_outcome").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "mirlist"
    displayKey: "value"
    source: substringMatcher(mirlist)

# matrix query
  $(".typeahead_gene_matrix").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "genelist"
    displayKey: "value"
    source: substringMatcher(genelist)

  $(".typeahead_protein_matrix").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "proteinlist"
    displayKey: "value"
    source: substringMatcher(proteinlist)

  $(".typeahead_mir_matrix").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "mirlist"
    displayKey: "value"
    source: substringMatcher(mirlist)

# 2pt target/genomic query
# first set
  $(".typeahead_gene_targ_genomic1").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "genelist"
    displayKey: "value"
    source: substringMatcher(genelist)

  $(".typeahead_protein_targ_genomic1").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "proteinlist"
    displayKey: "value"
    source: substringMatcher(proteinlist)

  $(".typeahead_mir_targ_genomic1").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "mirlist"
    displayKey: "value"
    source: substringMatcher(mirlist)

# second set
  $(".typeahead_gene_targ_genomic2").typeahead
    hint: false
    highlight: true
    minLength: 1
  ,
    name: "genelist"
    displayKey: "value"
    source: substringMatcher(genelist)

  $(".typeahead_mir_targ_genomic2").typeahead
    hint: true
    highlight: true
    minLength: 1
  ,
    name: "mirlist"
    displayKey: "value"
    source: substringMatcher(mirlist)

  return



