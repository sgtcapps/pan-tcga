$ ->
  # each chart container gets indexed, loop through each chart container to pre-load each chart
  # get chart data array from data attribute "chart-data-array"
  chart_count = $("#chart_count").data("chart-count")
  for idx in [0...chart_count] by 1
    #console.log "idx = " + idx
    cols_vals = $("#pie_chart_container_" + idx).data("chart-data-array")
    #console.log cols_vals
    $("#pie_chart_container_" + idx).highcharts
      chart:
        plotBackgroundColor: null
        plotBorderWidth: 1
        plotShadow: false
        marginBottom: 60
        width: 540
        height: 350

      title: false

      credits: false 

      tooltip:
        useHTML: true
        style:
          fontSize: "14px"

        headerFormat: "<table class=\"table table-bordered table-condensed\"><tr><th colspan=\"2\" style=\"text-align:center\">{point.key}</th></tr>"
        pointFormat: "<tr><td style=\"text-align:center;\">Count</td><td>Percent</td></tr> <tr><td style=\"text-align:center;\">{point.y}</td><td style=\"text-align:center\">{point.percentage:.1f}%</td></tr>"
        footerFormat: "</table>"

      plotOptions:
        pie:
          allowPointSelect: true
          cursor: "pointer"
          dataLabels:
            enabled: true
            format: "<b>{point.name}</b>: {point.percentage:.1f} %"
            style:
              color: (Highcharts.theme and Highcharts.theme.contrastTextColor) or "black"
          showInLegend: true

      series: [
        type: "pie"
        name: "High Level Summaries Visual Data"
        data: cols_vals
      ]

  return