# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $("#hint a").tooltip()

$ ->
  $("#data_information").dataTable 
    #sPaginationType: "bootstrap"
    bJQueryUI: false
    bProcessing: true
    bAutoWidth:false 
    aaSorting: [[0, "asc" ]]
    iDisplayLength: 25
    aLengthMenu: [[25, 50, 100, 200, -1], [25, 50, 100, 200, "All"]]

$ ->
  $("#target_selection").dataTable 
    sPaginationType: "bootstrap"
    bJQueryUI: false
    bProcessing: true
    bAutoWidth:false 
    aaSorting: [[0, "asc" ]]
    iDisplayLength: 15
    aLengthMenu: [[15, 25, 50, 100, 200, -1], [15, 25, 50, 100, 200, "All"]]


