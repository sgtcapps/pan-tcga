// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
// require dataTables/jquery.dataTables.bootstrap3  *removed this require statement from final line before .tree
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap/dropdown
//= require bootstrap/carousel
//= require bootstrap/modal
//= require bootstrap/collapse
//= require bootstrap/tooltip
//= require bootstrap/tab
//= require bootstrap-typeahead-rails
//= require highcharts
//= require highcharts/modules/data.js
//= require highcharts/modules/exporting.js
//= require highcharts/highcharts-more
//= require dataTables/jquery.dataTables
//= require_tree .

$(function() {
  $("#swap-div").click(function() {
    var div2;
    div2 = $("#swap-div").data("div2");
    $("#swap-div").fadeOut();
    $("#" + div2).show();
    return false;
  });
});

