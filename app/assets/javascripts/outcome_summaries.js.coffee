# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  pathname = window.location.pathname
#  console.log pathname
  if pathname == '/outcome_summary' 
    $.ajax
      type: "POST"
      cache: false
  $(".plus_minus:contains('+')").addClass "pinkbg"
  $(".plus_minus:contains('-')").addClass "bluebg"
  $(".plus_minus").text (i, t) ->
    t.replace(/(\-)/, htmlDecode("&darr;")).replace /(\+)/, htmlDecode("&uarr;")

htmlDecode = (value) ->
  $("<div/>").html(value).text()

# the tables corresponding to target have varying columns, so each
# one needs it's own dataTable function
# the aoColumns need to correspond to the number of columns
# the outcome tables generated from a query form need two extra columns
# for cancer type and clinical parameter
$ ->
  if $("#outcome_summary_table_genes").data("formqueryrequest") == true
    aocols = [
      null
      null    
      null
      null
      { sType: "nonemptyvalue" }
      { sType: "nonemptyvalue" }
      { sType: "nonemptyvalue" }
      { sType: "nonemptyvalue" }
      null
    ]
  else
    aocols = [
      null
      null
      { sType: "nonemptyvalue" }
      { sType: "nonemptyvalue" }
      { sType: "nonemptyvalue" }
      { sType: "nonemptyvalue" }
      null
    ]
  $("#outcome_summary_table_genes").dataTable 
    sPaginationType: "bootstrap"
    bJQueryUI: false
    bProcessing: true
    bAutoWidth:false 
    aaSorting: [[0, "asc" ]]
    aoColumns: aocols 
    iDisplayLength: 15
    aLengthMenu: [[15, 25, 50, 75, 100, -1], [15, 25, 50, 75, 100, "All"]]

$ ->
  if $("#outcome_summary_table_proteins").data("formqueryrequest") == true
    aocols = [
      null
      null
      null
      null
      { sType: "nonemptyvalue" }
      null
    ]
  else
    aocols = [
      null
      null
      { sType: "nonemptyvalue" }
      null
    ]
  $("#outcome_summary_table_proteins").dataTable 
    sPaginationType: "bootstrap"
    bJQueryUI: false
    bProcessing: true
    bAutoWidth:false 
    aaSorting: [[0, "asc" ]]
    aoColumns: aocols
    iDisplayLength: 15
    aLengthMenu: [[15, 25, 50, 75, 100, -1], [15, 25, 50, 75, 100, "All"]]

$ ->
  if $("#outcome_summary_table_mirs").data("formqueryrequest") == true
    aocols = [
        null
        null
        null
        null
        { sType: "nonemptyvalue" }
        #null
        { sType: "nonemptyvalue" }
        null
        #null
        #null
      ]
  else
    aocols = [
      null
      null
      { sType: "nonemptyvalue" }
      #null
      { sType: "nonemptyvalue" }
      null
      #null
      #null
    ]
  $("#outcome_summary_table_mirs").dataTable 
    sPaginationType: "bootstrap"
    bJQueryUI: false
    bProcessing: true
    bAutoWidth:false 
    aaSorting: [[0, "asc" ]]
    aoColumns: aocols
    iDisplayLength: 15
    aLengthMenu: [[15, 25, 50, 75, 100, -1], [15, 25, 50, 75, 100, "All"]]

# functions to handle sorting the values where empty values exist in rows
jQuery.fn.dataTableExt.oSort["nonemptyvalue-asc"] = (x, y) ->
  retVal = undefined
  #x = $.trim(x)
  #y = $.trim(y)
  x = parseInt(x)
  y = parseInt(y)
  if x is y
    retVal = 0
  else if x is "" or x is "&nbsp;" or isNaN(x) 
    retVal = 1
  else if y is "" or y is "&nbsp;" or isNaN(y) 
    retVal = -1
  else if x > y
    retVal = 1
  else 
    retVal = -1
  retVal

jQuery.fn.dataTableExt.oSort["nonemptyvalue-desc"] = (y, x) ->
  retVal = undefined
  #x = $.trim(x)
  #y = $.trim(y)
  x = parseInt(x)
  y = parseInt(y)
  if x is y
    retVal = 0
  else if x is "" or x is "&nbsp;" or isNaN(x) 
    retVal = -1
  else if y is "" or y is "&nbsp;" or isNaN(y) 
    retVal = 1
  else if x > y
    retVal = 1
  else 
    retVal = -1
  retVal

# for status tables
$ ->
  $("#gene_frequency_cn").dataTable 
    sPaginationType: "bootstrap"
    bJQueryUI: false
    bProcessing: true
    bAutoWidth: true
    aaSorting: [[0, "asc" ]]
    aLengthMenu: [
        [-1],
        ["All"]
    ],
    iDisplayLength: -1

$ ->
  $("#gene_frequency_mut").dataTable 
    sPaginationType: "bootstrap"
    bJQueryUI: false
    bProcessing: true
    bAutoWidth: true
    aaSorting: [[0, "asc" ]]
    aLengthMenu: [
        [-1],
        ["All"]
    ],
    iDisplayLength: -1

$ ->
    cols_vals = $("#pie_chart_container").data("chart-data-array")
    this_title = $("#pie_chart_container").data("title")
    $("#pie_chart_container").highcharts
      chart:
        plotBackgroundColor: null
        plotBorderWidth: 0
        plotShadow: false
        #marginLeft: 0
        marginBottom: 0
        marginTop: 28
        #spacingLeft:20
        #spacingBottom:2
        width: 463
        height: 250

      legend: false

      title:
        text: 
          this_title

      credits: false 

      tooltip:
        useHTML: true
        style:
          fontSize: "14px"

        headerFormat: "<table class=\"table table-bordered table-condensed\"><tr><th colspan=\"2\" style=\"text-align:center\">{point.key}</th></tr>"
        pointFormat: "<tr><td style=\"text-align:center;\">Count</td><td>Percent</td></tr> <tr><td style=\"text-align:center;\">{point.y}</td><td style=\"text-align:center\">{point.percentage:.1f}%</td></tr>"
        footerFormat: "</table>"

      plotOptions:
        pie:
          allowPointSelect: true
          cursor: "pointer"
          dataLabels:
            enabled: true
            useHTML: true
            format: "<b>{point.name}</b>: {point.percentage:.1f} %"
            style:
              color: (Highcharts.theme and Highcharts.theme.contrastTextColor) or "black"
          #showInLegend: true

      series: [
        type: "pie"
        name: "High Level Summaries Visual Data"
        data: cols_vals
      ]

  return




