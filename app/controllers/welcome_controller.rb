class WelcomeController < ApplicationController
  def index
    reset_session
    @genes = Gene.pluck(:name)
    @proteins = Protein.pluck(:name)
    @mirs = Mir.pluck(:name)  	
  end
end
