class HighLevelSummariesController < ApplicationController

  def index
    (@data_set_id, @high_level_genomic_id) = get_common_ids
    @cancer_type = nil
    @data_set = DataSet.find(@data_set_id)
    @high_level_genomic = HighLevelGenomic.find(@high_level_genomic_id)
    @clinical_parameter = nil
    conditions = {
      data_set_id: @data_set.id, 
      high_level_genomic_id: @high_level_genomic.id }
    if params[:cancer_type_id] # set on initial choice on welcome page
      cancer_type_id = params[:cancer_type_id] || session[:cancer_type_id]
      @cancer_type = CancerType.find(cancer_type_id)
      conditions.merge!({cancer_type_id: @cancer_type.id}) 
    elsif params[:clinical_parameter_id] # set on initial choice on welcome page
      clinical_parameter_id = params[:clinical_parameter_id] || session[:clinical_parameter_id]
      @clinical_parameter = ClinicalParameter.find(clinical_parameter_id)
      conditions.merge!({clinical_parameter_id: @clinical_parameter.id})
    end # /check initial params - cancer_type_id or clinical_parameter_id 

    # set session variables
    set_custom_session_variables
 
    # conditions depend on initial choice at welcome page

    #excludes = CancerType::ID_EXCLUDES  # exclude records with cancer_type_id for COAD AND READ
    high_level_summaries_table = Arel::Table.new(:high_level_summaries)
    # For now exclude DaysToDeath (clinical_parameter_id: 2)
    #@high_level_summaries = HighLevelSummary.where.not(clinical_parameter_id: 2).where(high_level_summaries_table[:cancer_type_id].not_in excludes).includes(:cancer_type, :data_set, :high_level_genomic, :clinical_parameter, :high_level_summary_values, :target_set).where(conditions)
    @high_level_summaries = HighLevelSummary.includes(:cancer_type, :data_set, :high_level_genomic, :clinical_parameter, :high_level_summary_values, :target_set).where(conditions)

    @high_level_summaries_target_info_hash = Hash.new { |hash, key| hash[key] = Hash.new { |hash, key| hash[key] = Array.new } } 

    @high_level_summaries.each do |summary|
      target_vals = [summary.target_set.name, 
                     summary.high_level_summary_values[0].num_samples, 
                     summary.high_level_summary_values[0].num_rank_target_sets]

      # fill target_set hash: group cancer_types, or, clinical parameters/clinical parameter sub cats # and concatenate the respective target sets (TargetGene, AllmiR, Protein)
      if @cancer_type
        if summary.clinical_parameter_sub_cat_id
          @high_level_summaries_target_info_hash[summary.clinical_parameter.name][summary.clinical_parameter_sub_cat.name] << target_vals
        else # no sub_cat
          @high_level_summaries_target_info_hash[summary.clinical_parameter.name][nil] << target_vals 
        end
      elsif @clinical_parameter
        if summary.clinical_parameter_sub_cat_id
          @high_level_summaries_target_info_hash[summary.cancer_type.abbreviation][summary.clinical_parameter_sub_cat.name] << target_vals
        else # no sub_cat
          @high_level_summaries_target_info_hash[summary.cancer_type.abbreviation][nil] << target_vals 
        end
      end # /cancer_type or clinical_parameter
    end # /iterate high_level_summaries

    # for debug
    #@value_count = ClinicalParameterValue.where(cancer_type_id: 8, data_set_id: 5, clinical_parameter_id: 3).group(:clin_param_value).count 
    #@chunks100 = chunks(500, @value_count.to_a)  
  end # /index

  def query_clin_param_vals
 
    #conditions = {clinical_parameter_id: 3}
    #@gender = ClinicalParameterValue.includes(:cancer_type, :sample, :clinical_parameter).where(conditions).limit(35)
    #render text: @target_flags 
  end 
  
end
