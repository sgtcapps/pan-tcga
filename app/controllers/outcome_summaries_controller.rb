class OutcomeSummariesController < ApplicationController

  def index
    #render text: params

    (@data_set_id, @high_level_genomic_id) = get_common_ids    
    # make sure query is not getting sub cat unless it's passed as a param
    session[:clinical_parameter_sub_cat_id] = nil
    #extra_conditions = ["outcome_summaries.clinical_parameter_id NOT IN (?)", [2]] # temp "hack", don't show records for days to death
    # set ids from various inputs
    if params[:query_for_outcome_summary]
      #render text: ['query', params, "session cancer type ID = #{session[:cancer_type_id]}", "session clinical parameter ID = #{session[:clinical_parameter_id]}"]
      target_set_id         = params[:query_for_outcome_summary][:target_set_id]
      target_class_name     = get_target_class_name(target_set_id.to_i) 
      if target_set_id == '1'    
        @target_name = params[:query_for_outcome_summary][:gene_name]
      elsif target_set_id == '2'
        @target_name = params[:query_for_outcome_summary][:protein_name]
      elsif target_set_id == '3'
        @target_name = params[:query_for_outcome_summary][:mir_name]
      end
      @target               = target_class_name.constantize.find_by_name(@target_name)
      target_id             = @target.id unless @target.blank?
      @query_for_outcome_summary = true
      @data_set = DataSet.find(@data_set_id)
      @target_set = TargetSet.find(target_set_id)
      @high_level_genomic = HighLevelGenomic.find(@high_level_genomic_id)
      @status = target_class_name.constantize.find(target_id).status unless @target.blank?
      # :target is gene, protein or mir, using polymorphic model
      unless @target.blank?
        @outcome_summaries = OutcomeSummary.includes(:target).where(
                               data_set_id: @data_set.id,                         
                               target_set_id: @target_set.id,
                               target_type: target_class_name,
                               target_id: target_id,                                                         
                             )
      end
      redirect_to root_url(query_error: 'target name input') if @target.blank?
    else
      #render text:  ['not query', params, "session cancer type ID = #{session[:cancer_type_id]}", "session clinical parameter ID = #{session[:clinical_parameter_id]}"]
      cancer_type_id = params[:cancer_type_id] ? params[:cancer_type_id] : session[:cancer_type_id]
      target_set_id = params[:target_set_id] ? params[:target_set_id] : session[:target_set_id] 
      clinical_parameter_id = params[:clinical_parameter_id] ? params[:clinical_parameter_id] : session[:clinical_parameter_id]
      clin_param_sub_cat_id = params[:clin_param_sub_cat_id] ? params[:clin_param_sub_cat_id] : nil
      @cancer_type = CancerType.find(cancer_type_id)
      @data_set = DataSet.find(@data_set_id)
      @target_set = TargetSet.find(target_set_id)
      @high_level_genomic = HighLevelGenomic.find(@high_level_genomic_id)
      @clinical_parameter = ClinicalParameter.find(clinical_parameter_id)
      @clinical_parameter_sub_cat = ClinicalParameterSubCat.find(clin_param_sub_cat_id) unless clin_param_sub_cat_id.blank?
      # :target is gene, protein or mir, using polymorphic model
      @outcome_summaries = OutcomeSummary.includes(:target).where(
                             cancer_type_id: @cancer_type.id, 
                             data_set_id: @data_set.id,                         
                             target_set_id: @target_set.id,
                             clinical_parameter_id: @clinical_parameter.id,
                             clinical_parameter_sub_cat_id: (@clinical_parameter_sub_cat ? @clinical_parameter_sub_cat.id : nil) 
                           )

      @chart_data = high_level_data_analysis(@cancer_type.id, @clinical_parameter.id, @target_set.id) 
    end

    # set session variables
    set_custom_session_variables
    
    # get frequency data
    if target_class_name && target_class_name.match('Gene')
      @gene_cn_frequency = get_gene_frequency('CN', target_id)
      @gene_mut_frequency = get_gene_frequency('Mut', target_id)
    end 

  end

  def high_level_data_analysis(ct_id, cp_id, ts_id)
      cancer_type_id = ct_id
      data_set_id    = session[:data_set_id]
      clin_param_id = cp_id
      target_set_id = ts_id

      @chart_data = get_clin_param_chart_data(cancer_type_id, data_set_id, clin_param_id, target_set_id)

  end

  def get_clin_param_chart_data(cancer_type_id, data_set_id, clin_param_id, target_set_id)
    target_flag = get_target_flag(target_set_id)
    conditions = {cancer_type_id: cancer_type_id, data_set_id: data_set_id, clinical_parameter_id: clin_param_id, target_flag => 1}
    clinical_parameter_values = ClinicalParameterValue.where(conditions)
    # return hash of aggregated column and it's count
    orig_value_count = ClinicalParameterValue.where(conditions).group(:clin_param_value).count
    orig_value_count.freeze
    value_count = orig_value_count.dup 

    # save key/value pairs with na or '' and delete from value_count
    na = {}
    value_count.each do |col, count|
      if col.blank? || col.match(/(\[NA\]|\[NG\])/i)
        na[col] = count
        value_count.delete(col)
      end
    end

    value_count_array = []
    #if ClinicalParameter.find(clin_param_id).name.match('DaysToDeath') # check for clin params to chunk columns
    #if clin_param_id == 2
    #  value_count_array = chunks(365, value_count.to_a)
    #else
    #  value_count_array = value_count.to_a
    #end
    value_count_array = value_count.to_a

    total_num_samples = clinical_parameter_values.length
    invalid_num_samples = na.values.inject(:+)
    valid_num_samples = (invalid_num_samples ? (total_num_samples - invalid_num_samples) : total_num_samples)
    target_flags = clinical_parameter_values.pluck(:target_genes_flag, :all_mir_flag, :all_protein_flag)
   
    [value_count_array, total_num_samples, valid_num_samples, invalid_num_samples, target_flag]
  end # get_clin_param_chart_data

protected

  def get_gene_frequency(genomic, gene_id) 
    #excludes = CancerType::ID_EXCLUDES # exclude records with cancer_type_id for COAD AND READ
    if genomic.match('CN')   
      #frequency = GeneCnFrequency.includes(:cancer_type, :gene).where.not(:cancer_type_id => excludes).where(data_set_id: session[:data_set_id], gene_id: gene_id)
      frequency = GeneCnFrequency.includes(:cancer_type, :gene).where(data_set_id: session[:data_set_id], gene_id: gene_id)
    elsif genomic.match('Mut')
      #frequency = GeneMutFrequency.includes(:cancer_type, :gene).where.not(:cancer_type_id => excludes).where(data_set_id: session[:data_set_id], gene_id: gene_id)
      frequency = GeneMutFrequency.includes(:cancer_type, :gene).where(data_set_id: session[:data_set_id], gene_id: gene_id)
    end
    frequency
  end
 
end
