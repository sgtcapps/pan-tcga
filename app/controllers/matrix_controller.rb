class MatrixController < ApplicationController
  helper_method :matrix_data_analysis
  after_action :clear_hashes
  
  #################################
  ## INDEX OF FUNCTIONS(ACTIONS) ##
  #################################
  # index
  # target_genomic_by_target_genomic
  # get_matrix_data
  # get_clinical_parameter_values
  # get_clin_param_value_type_samples_hash
  # get_target_type_value_objects
  # get_clin_param_category_target_type_values_hash
  # matrix_data_analysis
  # prepare_boxplot_data
  # prepare_histogram_data
  # get_single_val_boxplot_data
  # get_boxplot_data
  # calc_median
  # get_histogram_data
  # random_string
  # clear hashes

  def index
    #render text: params    
    (@data_set_id, @high_level_genomic_id) = get_common_ids
    # make sure query is not getting sub cat unless it's passed as a param
    session[:clinical_parameter_sub_cat_id] = nil
    # set ids from various inputs
    if params[:query_for_matrix]
      @query_for_matrix     = true
      cancer_type_id        = params[:query_for_matrix][:cancer_type_id]
      target_set_id         = params[:query_for_matrix][:target_set_id]
      @target_type          = get_target_class_name(target_set_id.to_i)
      @target_type_readable = get_target_type_readable_name(target_set_id.to_i)      
      if target_set_id == '1'    
        @target_name = params[:query_for_matrix][:gene_name]
      elsif target_set_id == '2'
        @target_name = params[:query_for_matrix][:protein_name]
      elsif target_set_id == '3'
        @target_name = params[:query_for_matrix][:mir_name]
      end
      clinical_parameter_id = params[:query_for_matrix][:clinical_parameter_id]
    else
      cancer_type_id        = params[:cancer_type_id] ? params[:cancer_type_id] : session[:cancer_type_id]
      target_set_id         = session[:target_set_id]
      @target_type          = params[:target_type] # Gene, Protein, Mir
      @target_type_readable = get_target_type_readable_name(target_set_id.to_i)   
      @target_name          = params[:target_name] # Gene, Protein or Mir name
      #target_id             = ''
      clinical_parameter_id = params[:clinical_parameter_id] ? params[:clinical_parameter_id] : session[:clinical_parameter_id]
      clin_param_sub_cat_id = params[:clin_param_sub_cat_id] ? params[:clin_param_sub_cat_id] : nil      
    end

    @cancer_type = CancerType.find(cancer_type_id)
    @data_set = DataSet.find(@data_set_id)
    @target_set = TargetSet.find(target_set_id)
    @high_level_genomic = HighLevelGenomic.find(@high_level_genomic_id)
    @clinical_parameter = ClinicalParameter.find(clinical_parameter_id)
    @clinical_parameter_sub_cat = ClinicalParameterSubCat.find(clin_param_sub_cat_id) unless clin_param_sub_cat_id.blank?

    target_flag = get_target_flag(@target_set.id)
    target_id_attr = @target_type.downcase + '_id'
    target_value_attr = @target_type.downcase + '_value' # a field in target type values table pertaining to the type
    target_table = 'target_' + @target_type.downcase + '_values'
    @target = @target_type.constantize.find_by_name(@target_name) # Object - Gene, Protein, Mir; needs to be instance var(?)
    target_type_value_class_name = 'Target' + @target_type + 'Value'

    if @target.blank?
      redirect_to root_url(query_error: 'target name input') 
    else
      @cp_conditions = {
        cancer_type_id: @cancer_type.id,
        data_set_id: @data_set.id,
        clinical_parameter_id: @clinical_parameter.id,
        target_flag => 1
      }
      # return the clinical parameter categories and clinical param value records 
      (@clinical_parameter_value_categories, @clinical_parameter_values) = get_clinical_parameter_values(@cp_conditions)
      
      # temp cache of clin param categories with their sample ids 
      # will be used in each panel request for data view
      @uniq_clin_param_samples_hash_cache_name = random_string
      session[:clin_param_samples_hash_cache_name] = @uniq_clin_param_samples_hash_cache_name
      @clin_param_value_category_samples_hash = Rails.cache.fetch(@uniq_clin_param_samples_hash_cache_name, :expires_in => 10.minutes) do
        get_clin_param_value_category_samples_hash(@data_set.id,
          @clinical_parameter_value_categories, 
          @clinical_parameter_values)
      end
       
      @genomic_id_arr = get_target_type_value_objects(cancer_type_id, @data_set_id, @target.id, target_type_value_class_name, target_id_attr, @clin_param_value_category_samples_hash)
      
      # set session variables
      set_custom_session_variables
    end # target blank check

  end # /index

  def target_genomic_by_target_genomic
    #render text: params.inspect
    (@data_set_id, @high_level_genomic_id) = get_common_ids
    cancer_type_id = params[:query_for_target_genomic][:cancer_type_id]
    target_set_id  = params[:query_for_target_genomic][:target_set_id]
    target_set_id2 = params[:query_for_target_genomic][:target_set_id2]
    genomic_id     = params[:query_for_target_genomic][:genomic_id]
    genomic_id2    = params[:query_for_target_genomic][:genomic_id2]
    @target_type   = get_target_class_name(target_set_id.to_i)
    @target_type2   = get_target_class_name(target_set_id2.to_i)
    
    if target_set_id == '1'    
      @target_name = params[:query_for_target_genomic][:gene_name]
    elsif target_set_id == '2'
      @target_name = params[:query_for_target_genomic][:protein_name]
    elsif target_set_id == '3'
      @target_name = params[:query_for_target_genomic][:mir_name]
    end
    
    if target_set_id2 == '1'    
      @target_name2 = params[:query_for_target_genomic][:gene_name2]
    elsif target_set_id2 == '3'
      @target_name2 = params[:query_for_target_genomic][:mir_name2]
    end    

    @cancer_type = CancerType.find(cancer_type_id)
    @data_set = DataSet.find(@data_set_id)
    @target_set = TargetSet.find(target_set_id)
    @target_set2 = TargetSet.find(target_set_id2)
    @high_level_genomic = HighLevelGenomic.find(@high_level_genomic_id)
    @genomic = Genomic.find(genomic_id)
    @genomic2 = Genomic.find(genomic_id2)    

    target_id_attr = @target_type.downcase + '_id'
    target_id_attr2 = @target_type2.downcase + '_id'
    target_value_attr = @target_type.downcase + '_value' # a field in target type values table pertaining to the type
    target_value_attr2 = @target_type2.downcase + '_value' # a field in target type values table pertaining to the type
    target_table = 'target_' + @target_type.downcase + '_values'
    target_table2 = 'target_' + @target_type2.downcase + '_values'
    @target = @target_type.constantize.find_by_name(@target_name) # Object - Gene, Protein, Mir
    @target2 = @target_type2.constantize.find_by_name(@target_name2) # Object - Gene, Protein, Mir
    target_type_value_class_name = 'Target' + @target_type + 'Value'
    target_type_value_class_name2 = 'Target' + @target_type2 + 'Value'
    
    @target_genomic_both_hash = Hash.new { |hash, key| hash[key] = Hash.new { |hash, key| hash[key] = Array.new } }
    @target_genomic_both_hash['cancer_type'][@cancer_type.abbreviation] = nil
    @target_genomic_both_hash['genomic1'][genomic_id] = nil
    @target_genomic_both_hash['genomic2'][genomic_id2] = nil
    @target_genomic_both_hash['target1'][@target_name] = nil
    @target_genomic_both_hash['target2'][@target_name2] = nil

    # set session variables
    set_custom_session_variables
=begin
    render text: [
      [CancerType.find(cancer_type_id).abbreviation, target_id_attr, target_value_attr, target_table, @target_type, @target_name, @target, target_type_value_class_name, Genomic.find(genomic_id).name],
      [CancerType.find(cancer_type_id).abbreviation, target_id_attr2, target_value_attr2, target_table2, @target_type2, @target_name2, @target2, target_type_value_class_name2, Genomic.find(genomic_id2).name]
    ]
=end

    if BOXPLOT_GENOMIC_IDS.include?(genomic_id.to_i)
          
      # query 1
      # get records for pair 1 - will need samples and values
      query1_conditions = {
        target_id_attr => @target.id, 
        cancer_type_id: cancer_type_id, 
        data_set_id: @data_set_id, 
        genomic_id: genomic_id
      }
      target_genomic1 = target_type_value_class_name.constantize.where(query1_conditions)
      query1_sample_ids = target_genomic1.pluck(:sample_id).sort
      
      # query 2
      # get records for pair 2. Map categories to samples that are in query 1 result samples
      @genomic2_categories = get_genomic_value_types(genomic_id2.to_i)      
      @target_genomic2_categories_samples_hash = {}
      query2_conditions = {        
        target_id_attr2 => @target2.id,
        cancer_type_id: cancer_type_id,  
        data_set_id: @data_set_id, 
        genomic_id: genomic_id2
      }
      @genomic2_categories.each do |genomic2_category|
        # query for records where target type values match genomic categories (target type values) from 2nd query,
        # ie. records with CN or MAF and sample is in query1 result
        target_genomic2 = target_type_value_class_name2.constantize.where(target_value_attr2 => genomic2_category).where(query2_conditions).where(["sample_id IN (?)", query1_sample_ids])
        query2_sample_ids = target_genomic2.pluck(:sample_id)
        # key is the genomic category from 2nd query, value is the 2nd query result, will use the samples to get the related values in the first query result
        @target_genomic2_categories_samples_hash[genomic2_category] = query2_sample_ids
      end
      
      # query 3 
      # get records from query 1 result
      # map categories for query 2 to values from query 1 where 
      # query 1 samples match query 2 samples for that genomic category
      # basically we want the genomic categories from query 2 (CN: -2,-1,0,1,2; MAF: 0,1) and values from query1 (continuous floats)
      @target_genomic2_cats_target_genomic1_values_hash = {}
      @target_genomic2_categories_samples_hash.each do |genomic2_category, q2_sample_ids|
        genomic1_values = target_genomic1.where(["sample_id IN (?)", q2_sample_ids]).pluck(target_value_attr).join(',')
        @target_genomic2_cats_target_genomic1_values_hash[genomic2_category] = genomic1_values
      end      
    else # process for barcharts
      unless @target.blank? || @target2.blank?    
        query1 = "select
                    group_concat(sample_id) as sampleids, 
                    #{target_value_attr} as targval,
                    count(#{target_value_attr}) as targvalcount
                  from #{target_table} 
                  where 
                    #{target_id_attr}=#{@target.id} and 
                    cancer_type_id=#{cancer_type_id} and
                    data_set_id=#{@data_set_id} and 
                    genomic_id=#{genomic_id} 
                  group by #{target_value_attr}"
          @target_genomic1 = target_type_value_class_name.constantize.find_by_sql(query1)
          @target_genomic1.each do |tg1|
            query2 = "select
                        group_concat(sample_id) as sampleids,
                        #{target_value_attr2} as targval2, 
                        count(#{target_value_attr2}) as targvalcount2
                      from #{target_table2} 
                      where 
                        sample_id IN (#{tg1.sampleids}) and
                        #{target_id_attr2}=#{@target2.id} and
                        cancer_type_id=#{cancer_type_id} and  
                        data_set_id=#{@data_set_id} and 
                        genomic_id=#{genomic_id2}
                      group by #{target_value_attr2}"
          target_genomic2 = target_type_value_class_name.constantize.find_by_sql(query2)
          # for key 'target_genomic2', set nested hash with target value category as key, query 2 results as value
          @target_genomic_both_hash['target_genomic2'][tg1.targval] = target_genomic2
        
          @genomic2_value_counts_hash = Hash.new {|h,k| h[k] = Array.new } 
          @target2_value_categories = []
          @target_genomic_both_hash['target_genomic2'].each do |genomic1_value_category, target_genomic_2_arr| 
            genomic1_val_cat_as_number = genomic1_value_category.to_i
            target_genomic_2_arr.each do |query2_result|            
              target2_value_category_as_number = query2_result.targval2.to_i            
              @target2_value_categories.push(target2_value_category_as_number) unless @target2_value_categories.include?(target2_value_category_as_number)
              @genomic2_value_counts_hash[genomic1_val_cat_as_number] << [target2_value_category_as_number,query2_result.targvalcount2]  
            end   
          end   
          @sorted_genomic2_value_counts_hash = Hash[@genomic2_value_counts_hash.sort_by {|k,v| k}]        
        end
      end # unless targets are blank?           
    end # if BOXPLOT_GENOMIC_IDS...
    
    # check for incomplete data return
    @target_name_input = '' 
    @boxplot_data = ''
    @barchart_data = ''
    
    if @target.blank? || @target2.blank?
      @target_name_input = false
    end

    if (@target_genomic2_cats_target_genomic1_values_hash.blank?) || (@target_genomic2_cats_target_genomic1_values_hash.values.all? &:blank?)
      @boxplot_data = false  
    else
      @boxplot_data = true
    end

    if @sorted_genomic2_value_counts_hash.blank?
      @barchart_data = false
    else  
      @barchart_data = true
    end
    
    if @target_name_input == false
      redirect_to root_url(query_error: 'target name input')
    elsif @boxplot_data == false && @barchart_data == false
      redirect_to root_url(query_error: 'data return')
    end

  end # target_genomic_by_target_genomic
  
  def get_matrix_data    
    @genomic_id = params[:genomic_id]
    @target_name = params[:target_name]
    chart_title = params[:chart_title]
    target_type = get_target_class_name(session[:target_set_id])
    target_type_value_class_name = 'Target' + target_type + 'Value'
    target_value_attr = target_type.downcase + '_value' # a field in target type values table pertaining to the type, ie. gene_id, protein_id, etc.
    target_table = 'target_' + target_type.downcase + '_values'
    target_id_attr = target_type.downcase + '_id'
    target_id = target_type.constantize.find_by_name(@target_name).id # from Object - Gene, Protein, Mir
    
    # get clin param categories with their sample ids from cache
    @uniq_clin_param_samples_hash_cache_name = session[:clin_param_samples_hash_cache_name] 
    @clin_param_value_category_samples_hash = Rails.cache.fetch(@uniq_clin_param_samples_hash_cache_name)

    # return hash of clinical parameter categories and associated target type value objects
    @clin_param_category_target_type_values_hash = get_clin_param_category_target_type_values_hash(session[:cancer_type_id], session[:data_set_id], @genomic_id, target_id, target_type_value_class_name, target_value_attr, target_table, target_id_attr, @clin_param_value_category_samples_hash)
    
    # sort keys
    @sorted_clin_param_category_target_type_values_hash = {}
    @clin_param_category_target_type_values_hash.sort.map do |clin_param_category, target_type_value| 
      @sorted_clin_param_category_target_type_values_hash[clin_param_category] = target_type_value
    end
        
    # prepare hash of data items for view - charts and tables
    @clin_par_val_categories = @sorted_clin_param_category_target_type_values_hash.keys
    
    # Each item is a one item array with just the target genomic values in the form of a string, 
    # so genomic_values_arr will be an array of strings of genomic values, 
    # one per clinical parameter category
    @genomic_values_arr = @sorted_clin_param_category_target_type_values_hash.values.map { |item| item[0] } 
    @genomic_values_arr.map! { |item| item.blank? ? item = "0" : item } # avoid split on nil value error further down in prepare_boxplot_data

    @chart_data_array = []
    @data_items = {}
    @data_items['chart_data'] = []
    @data_items['genomic_id'] = @genomic_id
    @data_items['categories'] = @clin_par_val_categories
    @data_items['chart_title'] = chart_title
    if BOXPLOT_GENOMIC_IDS.include?(@genomic_id.to_i)
      @data_items['chart_type'] = 'boxplot'
      @chart_data_array = prepare_boxplot_data(@genomic_id, @clin_par_val_categories, @genomic_values_arr)
    elsif @genomic_id == Genomic::MAF_ID.to_s # MAF/Mutation
      @data_items['chart_type'] = 'stacked_bar'
      @chart_data_array = prepare_histogram_data(@genomic_id, @clin_par_val_categories, @genomic_values_arr)
    elsif @genomic_id == Genomic::CN_ID.to_s # CN
      @data_items['chart_type'] = 'stacked_bar' #'histogram'  
      @chart_data_array = prepare_histogram_data(@genomic_id, @clin_par_val_categories, @genomic_values_arr)
    end
    @chart_data_array.each do |chart_data_hash| # one hash for each clinical parameter value type
      if @genomic_id == Genomic::CN_ID.to_s # CNGistic...
        @data_items['readable_categories'] = chart_data_hash['genomic_val_types'].map { |gvt| get_genomic_val_category_name_copy_number(gvt) }
      elsif @genomic_id == Genomic::MAF_ID.to_s # MAF/Mutation
        @data_items['readable_categories'] = chart_data_hash['genomic_val_types'].map { |gvt| get_genomic_val_category_name_mutation(gvt) } 
      end                
      @data_items['chart_data'].push(chart_data_hash['data_arr']) 
    end
    #render text: [@genomic_id, @clin_par_val_types, @genomic_values_arr]
    respond_to :js 
  end # /get_matrix_data  

protected

  def get_clinical_parameter_values(conditions)
    clinical_parameter_values = ClinicalParameterValue.includes(:cancer_type, :data_set, :clinical_parameter).where(conditions)

    clinical_parameter_value_types = clinical_parameter_values.pluck(:clin_param_value).uniq
    clinical_parameter_value_types.delete('[NA]')
    clinical_parameter_value_types.delete('NA')

    [clinical_parameter_value_types, clinical_parameter_values]
  end 

  def get_clin_param_value_category_samples_hash(data_set_id, clinical_parameter_value_categories, clinical_parameter_values)
    hash = {}
    clinical_parameter_value_categories.each do |clin_param_val_category|
      hash[clin_param_val_category] = clinical_parameter_values.where(data_set_id: data_set_id, clin_param_value: clin_param_val_category).pluck(:sample_id) #only get sample_ids associated with this clin par val category
    end
    hash
  end
  
  # get target genomic value records as queried from target_gene_values, target_protein_values, etc.
  def get_target_type_value_objects(cancer_type_id, data_set_id, target_id, target_type_value_class_name, target_id_attr, clin_param_value_type_samples_hash)
    arr = [] 
    clin_param_value_type_samples_hash.each do |clin_par_val_type, sample_ids|
      samples_ids_str = sample_ids.join(',')
      conditions = {
        sample_id: samples_ids_str,    
        target_id_attr => target_id,
        cancer_type_id: cancer_type_id,
        data_set_id: data_set_id 
      }
      genomic_ids = target_type_value_class_name.constantize.where(conditions).group(:genomic_id).pluck(:genomic_id)
      arr.push(genomic_ids)
    end
    arr.flatten.uniq
  end
  
  # Query TargetGeneValue, TargetProteinValue or TargetMirValue. 
  # Each nested array contains a target type value object. 
  # Target type value object has a string attribute: target_genomic_values,
  # a comma sep. concat of target genomic values.
  # Return hash to associate clinical parameter category with array of arrays of target_genomic_values
  def get_clin_param_category_target_type_values_hash(cancer_type_id, data_set_id, genomic_id, target_id, target_type_value_class_name, target_value_attr, target_table, target_id_attr, clin_param_value_category_samples_hash)
    hash = {}
    #target_type_value_objects = [] 
    clin_param_value_category_samples_hash.each do |clin_par_category, sample_ids|
      query = "select genomic_id, group_concat(#{target_value_attr}) as target_genomic_values from #{target_table} 
           where
             sample_id IN (#{sample_ids.join(',')}) and       
             #{target_id_attr}=#{target_id} and 
             cancer_type_id=#{cancer_type_id} and 
             data_set_id=#{data_set_id} and
             genomic_id=#{genomic_id}" 
 
      target_type_value_objects = target_type_value_class_name.constantize.find_by_sql(query)
      target_genomic_values_arr = target_type_value_objects.map { |target_type_value_obj| target_type_value_obj.target_genomic_values }
      hash[clin_par_category] = target_genomic_values_arr 
       
    end
    hash
  end # /get_clin_param_category_target_type_values_hash      

  # this function will determine which type of chart to display
  # 3rd param - genomic_values_arr is array of strings
  def matrix_data_analysis(genomic_id, categories, genomic_values_arr)
    # array of hashes for chart related data
    chart_data_arr = []
    #TODO: change this to use helper
    if genomic_id == Genomic::CN_ID # CNGistic2thres
      genomic_value_types = %w(-2 -1 0 1 2)    
    elsif genomic_id == Genomic::MAF_ID # MAF
      genomic_value_types = %w(0 1)     
    end
    genomic_values_arr.each_with_index do |values_string, index|
      category = categories[index]
      # each values_string is a string list of genomic values
      # chart_data_arr will get concatenation of hashes based on each 
      # clinical parameter category and it's list of genomic_values
      if BOXPLOT_GENOMIC_IDS.include?(genomic_id)
        values_arr = values_string.split(',').collect { |x| x.to_f.round(3) } 
        if values_arr.length <= 1 
          chart_data_arr << get_single_val_boxplot_data(genomic_id, category, values_arr)
        else
          chart_data_arr << get_boxplot_data(genomic_id, category, values_arr) 
        end
      elsif HISTOGRAM_GENOMIC_IDS.include?(genomic_id) 
        values_arr = values_string.split(',').collect { |x| x.to_i }
        chart_data_arr << get_histogram_data(genomic_id, category, values_arr, genomic_value_types)
      end
    end
    chart_data_arr
  end
  
  def prepare_boxplot_data(genomic_id, categories, genomic_values_arr)
    # array of hashes for chart related data
    chart_data_arr = []
    genomic_values_arr.each_with_index do |values_string, index|
      category = categories[index]      
      # each values_string is a string list of genomic values
      # chart_data_arr will get concatenation of hashes based on each 
      # clinical parameter category and it's list of genomic_values
      values_arr = values_string.split(',').collect { |x| x.to_f.round(3) } 
      if values_arr.length <= 1 
        chart_data_arr << get_single_val_boxplot_data(genomic_id, category, values_arr)
      else
        chart_data_arr << get_boxplot_data(genomic_id, category, values_arr) 
      end
    end
    chart_data_arr
  end
  
  # for both histogram and stacked bar
  def prepare_histogram_data(genomic_id, categories, genomic_values_arr)                    
    # array of hashes for chart related data
    chart_data_arr = []
    genomic_value_types = get_genomic_value_types(genomic_id.to_i) 
    genomic_values_arr.each_with_index do |values_string, index|
      category = categories[index]      
      # each values_string is a string list of genomic values
      # chart_data_arr will get concatenation of hashes based on each 
      # clinical parameter category and it's list of genomic_values
      values_arr = values_string.split(',').collect { |x| x.to_i }
      chart_data_arr << get_histogram_data(genomic_id, category, values_arr, genomic_value_types)
    end
    chart_data_arr
  end

  def get_single_val_boxplot_data(genomic_id, clinical_parameter_category, values)
    data_hash = {}
    data_hash['single_val'] = 'true'
    data_hash['values_length'] = values.length
    data_hash['genomic_name'] = Genomic.find(genomic_id).name 
    data_hash['clinical_parameter'] = clinical_parameter_category
    data_hash['data_arr'] = [nil,nil,values[0],nil,nil]

    data_hash
  end  

  def get_boxplot_data(genomic_id, clinical_parameter_category, values) # values will be array of floats
    data_hash = {}
    box_plot_data_array = Array.new(5)
    sorted_values = values.sort!
    require 'descriptive-statistics'
    stats = DescriptiveStatistics::Stats.new(sorted_values)
    median = stats.median.round(3)

    if sorted_values.length.odd?
      index_at_median = sorted_values.find_index(median)
      # get middle item
      q1_observations = sorted_values[0..index_at_median - 1]
      q3_observations = sorted_values[index_at_median + 1..sorted_values.length - 1]
    else # is even
      q1_observations = sorted_values[0..(sorted_values.length / 2) - 1]
      q3_observations = sorted_values[(sorted_values.length / 2)..sorted_values.length - 1]
    end
    q1_stats = DescriptiveStatistics::Stats.new(q1_observations)
    q3_stats = DescriptiveStatistics::Stats.new(q3_observations)
    q1 = q1_stats.median.round(3)
    q3 = q3_stats.median.round(3)

    # for lower and inner fences
    iqr = q3 - q1
    iqr = iqr.round(3)
    iqr_fence_distance = 1.5 * iqr
    iqr_fence_distance = iqr_fence_distance.round(3)
    lower_inner_fence = q1 - iqr_fence_distance
    lower_inner_fence = lower_inner_fence.round(3)
    upper_inner_fence = q3 + iqr_fence_distance
    upper_inner_fence = upper_inner_fence.round(3)

    minimum_in_fence = 0 # gt lower inner fence
    sorted_values.each do |x|
      if x > lower_inner_fence
        minimum_in_fence = x 
        break
      else
        minimum_in_fence = sorted_values.min
      end
    end

    maximum_in_fence = 0 # lt upper_inner_fence
    sorted_values.reverse.each do |x| #start from last element
      if x < upper_inner_fence
        maximum_in_fence = x 
        break
      else
        maximum_in_fence = sorted_values.max
      end
    end

    box_plot_data_arr = [
      minimum_in_fence,
      q1,
      median,
      q3,
      maximum_in_fence]

    # most of these value were for debugging
    #data_hash['sorted_values'] = sorted_values
    #data_hash['median'] = median
    #data_hash['index_at_median'] = index_at_median
    #data_hash['q1_observations'] = q1_observations
    #data_hash['q3_observations'] = q3_observations
    #data_hash['q1'] = q1
    #data_hash['q3'] = q3
    #data_hash['iqr'] = iqr
    #data_hash['iqr_fence_distance'] = iqr_fence_distance
    #data_hash['lower_inner_fence'] = lower_inner_fence
    #data_hash['upper_inner_fence'] = upper_inner_fence
    #data_hash['minimum_in_fence'] = minimum_in_fence
    #data_hash['maximum_in_fence'] = maximum_in_fence
    #data_hash['values_length'] = values.length
    data_hash['genomic_name'] = Genomic.find(genomic_id).name
    data_hash['clinical_parameter'] = clinical_parameter_category
    data_hash['data_arr'] = box_plot_data_arr
    
    data_hash
  end # get_boxplot_data
  
  def calc_median(sorted_array)
    len = sorted_array.length
    return (sorted_array[(len - 1) / 2] + sorted_array[len / 2]) / 2.0
  end

  def get_histogram_data(genomic_id, clinical_parameter, values, genomic_val_types) # values will be array of integers
    values_count_hash = {}
    # key is value type, value is count by groups of values
    values_count_hash = values.inject(Hash.new(0)) { |h, e| h[e] += 1 ; h }
    # returns nested arrays sorted by keys and their values 
    sorted_values_count_arr = values_count_hash.sort_by { |genomic_value_type, count| genomic_value_type }
    # rearrange nested arrays into two cols containing the arr[0] items (genomic value types) and arr[1] items (genomic value counts)
    genomic_type_count_arr = sorted_values_count_arr.transpose 
    types = genomic_type_count_arr[0].map { |i| i.to_s }
    counts = genomic_type_count_arr[1]
    formatted_types = types.clone
    formatted_counts = counts.clone

    unless formatted_types.length == genomic_val_types.length    
      genomic_val_types.each_with_index do |item, index|        
        if formatted_types[index] != item
          formatted_types.insert(index, 'BLANK')
          formatted_counts.insert(index, 'BLANK')
        end 
      end
    end

    data_hash = {}
    data_hash['genomic_name'] = Genomic.find(genomic_id).name
    data_hash['clinical_parameter'] = clinical_parameter
    data_hash['genomic_val_types'] = genomic_val_types
    data_hash['formatted_types'] = formatted_types
    data_hash['data_arr'] = formatted_counts      
     
    data_hash
  end # get_histogram_data
  
  def random_string
    string ||= "#{SecureRandom.urlsafe_base64}"
  end
  
  private
  
  def clear_hashes
    @target_genomic_both_hash.clear if @target_genomic_both_hash
    @genomic2_value_counts_hash.clear if @genomic2_value_counts_hash
    @target_genomic2_categories_samples_hash.clear if @target_genomic2_categories_samples_hash
    @target_genomic2_cats_target_genomic1_values_hash.clear if @target_genomic2_cats_target_genomic1_values_hash
  end

end
