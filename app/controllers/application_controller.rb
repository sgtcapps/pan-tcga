class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :reset_session

  helper_method [
    :get_common_ids,
    :get_table_array,
    :to_csv,
    :set_custom_session_variables,
    :get_target_flag,
    :get_target_class_name,
    :get_target_type_readable_name, 
    :get_genomic_value_types,
    :get_genomic_val_category_name_mutation,
    :get_genomic_val_category_name_copy_number,
    :chunks, 
  ]
  
protected

  # common ids
  def get_common_ids
    data_set_id = session[:data_set_id] ? session[:data_set_id] : DataSet.last.id   
    high_level_genomic_id = session[:high_level_genomic_id] ? session[:high_level_genomic_id] : HighLevelGenomic.find_by_category("Integrative").id
    [data_set_id, high_level_genomic_id]
  end    
    
  def get_table_array(path)
    require 'csv'
    table_array = CSV.read(path, {:col_sep => "\t"})
  end
  
  def to_csv(file_name, array)
    require 'csv'    
    file = CSV.generate do |csv|
        #csv << header if not header.blank?
        csv << array.shift
        array.map {|row| csv << row}
    end
    file
  end

  def set_custom_session_variables
    session[:cancer_type_id] = @cancer_type.id unless @cancer_type.blank?
    session[:data_set_id] = @data_set.id unless @data_set.blank?
    session[:high_level_genomic_id] = @high_level_genomic.id unless @high_level_genomic.blank?
    session[:target_set_id] = @target_set.id unless @target_set.blank?
    session[:clinical_parameter_id] = @clinical_parameter.id unless @clinical_parameter.blank?
    session[:clinical_parameter_sub_cat_id] = @clinical_parameter_sub_cat.id unless @clinical_parameter_sub_cat.blank?
  end
  
  def get_target_flag(target_set_id)
    target_flag = ''
    if target_set_id == 1 # TargetGenes
      target_flag = 'target_genes_flag'
    elsif target_set_id == 2 # Allprotein
      target_flag = 'all_protein_flag'
    elsif target_set_id == 3 # AllmiR
      target_flag = 'all_mir_flag'
    end
    target_flag
  end

  def get_target_class_name(target_set_id)
    target_class_name = 'Gene' if target_set_id == 1
    target_class_name = 'Protein' if target_set_id == 2
    target_class_name = 'Mir' if target_set_id == 3
    target_class_name
  end

  def get_target_type_readable_name(target_set_id)
    target_class_name = 'Gene' if target_set_id == 1
    target_class_name = 'Protein' if target_set_id == 2
    target_class_name = 'miR' if target_set_id == 3
    target_class_name
  end
  
  def get_genomic_value_types(id)
    if id == Genomic::CN_ID # CNGistic2thres
      genomic_value_types = %w(-2 -1 0 1 2)    
    elsif id == Genomic::MAF_ID # MAF
      genomic_value_types = %w(0 1)     
    end
    genomic_value_types
  end
  
  def get_genomic_val_category_name_mutation(genomic_category)
    genomic_cat_name = ''
    if genomic_category == '0' 
      genomic_cat_name = 'No Mutation'
    elsif genomic_category == '1'
      genomic_cat_name = 'Mutation'
    end
    genomic_cat_name
  end

  def get_genomic_val_category_name_copy_number(genomic_category)
    genomic_cat_name = ''
    if genomic_category == '-2' 
      genomic_cat_name = 'Homozygous deletion'
    elsif genomic_category == '-1'
      genomic_cat_name = 'Hemizygous deletion'
    elsif genomic_category == '0'
      genomic_cat_name = 'No change'
    elsif genomic_category == '1'
      genomic_cat_name = 'Gain'
    elsif genomic_category == '2'
      genomic_cat_name = 'Amplification'
    end
    genomic_cat_name
  end
  
  # put values into chunks in range by size
  # first param is chunk size, second is nested array of arrays
  # first item is value, second is the count
  # returns array of arrays, first item is the range of values, second is the count for that range
  def chunks(size, val_count)
    chunks = []
    vals = []
    i=1
    j=size
    val_count.sort! {|a,b| a[0].to_i <=> b[0].to_i}
    values = val_count.transpose[0].map {|val| val.to_i } # get all of element 0's, the values; convert to int

    until j >= values.last + size
      values.map {|v| vals << v  if (i..j).include? v} 
      chunks << [i.to_s + '-' + j.to_s, vals.inject(:+)] unless vals.blank? # first item is the range, second is total vals
      vals = []
      i+=size
      j+=size   
    end

    #chunk_hash = {
      #'val_count' => val_count,
      #'val_count.length' => val_count.length,
      #'values' => values,
      #'values.length' => values.length,
      #'values.last' => values.last,
      #'chunks' => chunks,
    #}
    chunks
  end
end
