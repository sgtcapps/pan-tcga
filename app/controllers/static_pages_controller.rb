class StaticPagesController < ApplicationController
  def faq
  end
  
  def about
    @versions = Version::ALLVERSIONS
  end
  
  def contact
  end
  
  def project_overview
  end
  
  def analysis_pipeline
  end
  
  def target_selection
    if Version::CURRENT_VERSION.version_number == '1.0'
      render 'target_selection_v1'
    else
      render 'target_selection'
    end
  end

  def tutorial_overview
    
  end

  def data_information
    # UNCOMMENT THESE FOR INITIAL LOAD OF DATA TABLE WITH 'data_information_query_table' partial
    # AFTERWARDS, COMMENT THEM BACK OUT. TOTAL QUERY TIME IS TOO SLOW
    #@cancer_types = CancerType.where.not(abbreviation: ['COAD', 'READ']).order(:abbreviation)
    #@samples = Sample.select(:id, :cancer_type_id)
    #@gene_values = TargetGeneValue.select(:sample_id, :cancer_type_id).group(:sample_id)
    #@protein_values = TargetProteinValue.select(:sample_id, :cancer_type_id).group(:sample_id)
    #@mir_values = TargetMirValue.select(:sample_id, :cancer_type_id).group(:sample_id)
    @cancer_types = CancerType.order(:abbreviation)
    @cancer_sample_counts = CancerSampleCount.all
  end
  
  def show_target_selection_file
    @filename = params[:filename]
    @table_array = get_target_selection_table(@filename)
    if @table_array.blank?
      respond_to do |format|
        format.html { 
            flash[:notice] = "Sorry, no file was found named #{@filename}"         
            redirect_to target_selection_path
          }
        end # /respond_to
    else
      render 'target_selection_table'
    end
  end
  
  def get_target_selection_table(fn)
    file_path = PanTcga::Application::config.data_file_root + [Version::APP_FILE_DIR, 'Selected_Target_List', fn].join('/')
    target_selection_table_array = get_table_array(file_path) if File.exists?(file_path)
    target_selection_table_array
  end
  
  def target_selection_send_data(filename = params[:filename])
    table_array = get_target_selection_table(filename)
    csv_file = to_csv(filename, table_array)
    send_data csv_file, :type => 'text/csv; charset=iso-8859-1; header=present', 
      :disposition => "attachment;filename=#{filename.gsub('.txt', '.csv')}"
  end
  
  def ppt_sendfile(filename = params[:filename])
    ppt_file = Rails.root + ['app', 'assets', 'docs', filename].join('/')
    send_file ppt_file, :type => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow', 
      :disposition => "attachment;filename=#{filename}"
  end
  
  def data_download
    @cancer_types = CancerType.order('name ASC')
  end

  def send_zip_file
    file_name = params[:matrix_cancer_type] + '.zip'
    file_path = PanTcga::Application::config.data_file_root + [Version::APP_FILE_DIR, 'ZippedFiles', file_name].join('/')
    #render text: "<h2>#{file_name} at #{file_path} unavailable, coming soon</h2>"
    send_file file_path, type: 'application/zip', disposition: "attachment;filename=#{file_name}", x_sendfile: true
  end

end
