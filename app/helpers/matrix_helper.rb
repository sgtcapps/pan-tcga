module MatrixHelper

  def get_genomic_readable_name(genomic_id)
    genomic_readable_name = Genomic.find(genomic_id).readable_name
    genomic_readable_name
  end
 
end
