module OutcomeSummariesHelper

  def format_pval(pval)
    #"%.3E" % pval
    "%.8f" % pval
  end

  def display_pvals(pvals)
    disp_pvals = pvals.map {|pval| format_pval(pval)}
    disp_pvals.join(', ')
  end
end
