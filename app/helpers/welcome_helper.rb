module WelcomeHelper

  def get_clinical_parameters_for_select
    #excludes = [2]  (Previously DaysToDeath, no longer in table)
    clinical_parameters_table = Arel::Table.new(:clinical_parameters)
    #clinical_parameters_without_excludes = ClinicalParameter.where(clinical_parameters_table[:id].not_in excludes)
    clinical_parameters_without_excludes = ClinicalParameter.all
    clinical_parameters_without_excludes
  end
  
  def get_genomic_types_for_select1
    #excludes = [12,13,5] # all methylations
    genomics_table = Arel::Table.new(:genomics)
    #genomics_without_excludes = Genomic.where(genomics_table[:id].not_in excludes)
    genomics_without_excludes = Genomic.all
    genomics_without_excludes
  end
  
  def get_genomic_types_for_select2
    includes = [4,5]   # Copy number, MAF  # Fix this later to not rely on id
    genomics_table = Arel::Table.new(:genomics)
    genomics_without_excludes = Genomic.where(genomics_table[:id].in includes)
    genomics_without_excludes
  end

end
