module ApplicationHelper
  
  def full_title(page_title)
    base_title = "The Stanford-Cancer Genome Atlas Portal"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end
  
  def flash_class(level)
    case level
        when :notice then "alert alert-danger"
        when :success then "alert alert-success"
        when :error then "alert alert-error"
        when :alert then "alert alert-error"
    end
  end
  
  def get_cancer_types_for_select
    excludes = %w(COAD READ)
    cancer_types_table = Arel::Table.new(:cancer_types)
    cancer_types_without_excludes = CancerType.where(cancer_types_table[:abbreviation].not_in excludes)
    cancer_types_without_excludes
  end

=begin  
  def breaking_word_wrap(text, *args)
    options = args.extract_options!
    unless args.blank?
     options[:line_width] = args[0] || 80
    end
    options.reverse_merge!(:line_width => 80)
    text = text.split(",").collect do |word|
     word.length > options[:line_width] ? word.gsub(/(.{1,#{options[:line_width]}})/, "\\1 ") : word
    end * " "
    text.split("\n").collect do |line|
     line.length > options[:line_width] ? line.gsub(/(.{1,#{options[:line_width]}})(\s+|$)/, "\\1\n").strip : line
    end * "\n"
  end
=end
  
end
