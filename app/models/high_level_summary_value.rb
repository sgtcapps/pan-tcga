class HighLevelSummaryValue < ActiveRecord::Base
  belongs_to :high_level_summary
  belongs_to :genomic
end
