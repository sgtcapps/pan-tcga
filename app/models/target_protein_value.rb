class TargetProteinValue < ActiveRecord::Base
  belongs_to :cancer_type
  belongs_to :data_set
  belongs_to :protein
  belongs_to :genomic
  belongs_to :sample  
end
