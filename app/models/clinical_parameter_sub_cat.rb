class ClinicalParameterSubCat < ActiveRecord::Base
  has_many :high_level_summaries

  validates :name, presence: true, uniqueness: true
end
