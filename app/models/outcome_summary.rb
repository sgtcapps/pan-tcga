class OutcomeSummary < ActiveRecord::Base
  belongs_to :cancer_type
  belongs_to :data_set
  belongs_to :target_set
  belongs_to :high_level_genomic
  belongs_to :genomic
  belongs_to :clinical_parameter
  belongs_to :clinical_parameter_sub_cat
  belongs_to :gene
  belongs_to :target, polymorphic: true

  def p_values
    p_vals = [pval_copy_number, pval_m_rna_expression, pval_methylation, pval_mutation, pval_mi_rna_expression, pval_protein_expression]
    p_vals.compact
  end

end
