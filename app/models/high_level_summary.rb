class HighLevelSummary < ActiveRecord::Base
  belongs_to :cancer_type
  belongs_to :data_set
  belongs_to :target_set
  belongs_to :clinical_parameter
  belongs_to :clinical_parameter_sub_cat
  belongs_to :high_level_genomic

  has_many :high_level_summary_values

end
