class DataSet < ActiveRecord::Base

	has_many :high_level_summaries
	has_many :clinical_parameter_values
  has_many :target_gene_values
  has_many :target_protein_values
  has_many :target_gene_values

	VALID_DATE_REGEX = /\A\d{4}-\d{2}-\d{2}/
	validates :analysis_date, presence: true, 
	                      format: VALID_DATE_REGEX,
	                      uniqueness: true

	ANALYSIS_DATE = self.last.analysis_date
end
