class Gene < ActiveRecord::Base
  has_many :outcome_summaries, as: :target
  has_many :target_gene_values
  has_many :gene_cn_frequencies
  has_many :gene_mut_frequencies
  
  validates :name, presence: true, uniqueness: true
end
