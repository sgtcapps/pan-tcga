class Sample < ActiveRecord::Base
  belongs_to :cancer_type
  belongs_to :data_set
  has_many   :clinical_parameter_values
  has_many   :clinical_parameters, through: :clinical_parameter_values
  has_many :target_gene_values
  has_many :target_protein_values
  has_many :target_gene_values

  #validates :sample_code, presence: true, uniqueness: true

end
