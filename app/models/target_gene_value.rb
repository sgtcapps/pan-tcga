class TargetGeneValue < ActiveRecord::Base
  belongs_to :cancer_type
  belongs_to :data_set
  belongs_to :gene
  belongs_to :genomic
  belongs_to :sample
end
