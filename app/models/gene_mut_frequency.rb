class GeneMutFrequency < ActiveRecord::Base
  belongs_to :cancer_type
  belongs_to :data_set
  belongs_to :target_set
  belongs_to :gene
end
