class Genomic < ActiveRecord::Base
  has_many :high_level_summary_values
  has_many :target_gene_values
  has_many :target_protein_values
  has_many :target_gene_values

  validates :name, presence: true, uniqueness: true
  
  CN_ID = 4
  MAF_ID = 5

  def readable_name
    (name_text.blank? ? name : name_text)
  end

end
