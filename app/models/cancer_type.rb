class CancerType < ActiveRecord::Base

  has_many :target_sets
  has_many :samples
  has_many :high_level_summaries
  has_many :clinical_parameter_values
  has_many :target_gene_values
  has_many :target_protein_values
  has_many :target_gene_values

  validates :name, presence: true, uniqueness: true
  validates :abbreviation, presence: true, uniqueness: true

  ID_EXCLUDES = self.where(abbreviation: ['COAD', 'READ']).pluck(:id)

  def abbr_name
  	abbreviation + '-' + name
  end

end
