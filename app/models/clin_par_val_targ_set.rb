class ClinParValTargSet < ActiveRecord::Base

  belongs_to :clinical_parameter_value
  belongs_to :target_set

end