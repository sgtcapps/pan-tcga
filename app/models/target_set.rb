class TargetSet < ActiveRecord::Base

  belongs_to :cancer_type

  has_many :samples
  has_many :high_level_summaries
  has_many :clin_par_val_targ_sets

  VALID_TARGETSET_REGEX = /\A[a-zA-Z0-9-]+\z/ #targetset token will be part of filename which uses "_" as seperator
  validates :name, presence: true, 
                   format: VALID_TARGETSET_REGEX,
                   uniqueness: true

  def flag_name
    id_to_flag = {1 => 'target_genes_flag', 2 => 'all_protein_flag', 3 => 'all_mir_flag'}
    return id_to_flag[id]
  end

  def klass_name
    id_to_class = {1 => 'Gene', 2 => 'Protein', 3 => 'Mir'}
    return id_to_class[id]
  end

  def target_text
    return (id == 3 ? 'miR' : klass_name)
  end
end
