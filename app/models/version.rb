class Version < ActiveRecord::Base
	belongs_to :data_set

	ALLVERSIONS = self.includes(:data_set).order("release_date DESC").all
  CURRENT_VERSION = self.last
  APP_FILE_DIR = (CURRENT_VERSION.version_number == '1.0' ? 'Version1' : 'Current')

  def pvalue_msg
    return (version_number == '1.0' ? 'p-values not available in this version': 'p=0 indicates p < 1.0e-08')
  end
end
