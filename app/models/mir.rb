class Mir < ActiveRecord::Base
  has_many :outcome_summaries, as: :target
  has_many :target_mir_values

  validates :name, presence: true, uniqueness: true
end
