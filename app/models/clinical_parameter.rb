class ClinicalParameter < ActiveRecord::Base
  has_many :high_level_summaries
  has_many :clinical_parameter_values
  has_many :samples, through: :clinical_parameter_values

  validates :name, presence: true, uniqueness: true
end
