class ClinicalParameterValue < ActiveRecord::Base
  belongs_to :cancer_type
  belongs_to :sample
  belongs_to :clinical_parameter
  belongs_to :data_set

  has_many :clin_par_val_targ_sets

  validates :clin_param_value, presence: true
end
