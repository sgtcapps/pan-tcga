require 'spec_helper'

describe ApplicationHelper do

  describe "full_title" do
    it "should include the page title" do
      expect(full_title("Thing")).to match(/Thing/)
    end

    it "should include the base title" do
      expect(full_title("About")).to match(/^About \| Stanford - Cancer Genome Atlas Analysis of Pan Cancers/)
    end

    it "should not include a bar for the home page" do
      expect(full_title("")).not_to match(/\|/)
    end
  end
  
  describe "test_test" do
    it "should include input" do
      expect(test_test("oops")).to match(/^oops I am a test/)
    end
  end
  
end