require 'spec_helper'

describe "Welcome type pages" do

  subject { page }

  describe "index page" do
    before { visit root_path }

    it { should have_content('Cancer Type') }
    it { should have_title(full_title('')) }
    it { should_not have_title('Home') }
  end
  
  # the long syntax 
  it "should have the right links on the layout" do
    visit root_path
    
    #click_link "Link Text" # for only link on page

    first(:link, "Home").click #for dupe links on same page
    expect(page).to have_title(full_title(''))
    #first(:link, "About").click
    #expect(page).to have_title(full_title('About'))
    first(:link, "FAQ's").click
    expect(page).to have_title(full_title('FAQ'))
    first(:link, "Contact").click
    expect(page).to have_title(full_title('Contact'))
    first(:link, "Project Overview").click
    expect(page).to have_title(full_title('Project Overview'))
    first(:link, "Analysis Pipeline").click
    expect(page).to have_title(full_title('Analysis Pipeline'))
    first(:link, "Gene Selection").click
    expect(page).to have_content('TCGA')
    first(:link, "Data Information").click
    expect(page).to have_title(full_title('Data Information'))
  end
  
end
