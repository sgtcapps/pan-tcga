require 'spec_helper'

describe CancerType do
    before do
      # needs to be entered correctly or it will fail
      @cancer_type = CancerType.new(name: "ThisTumor", abbreviation: "TT")
    end

  subject { @cancer_type }

  it { should respond_to(:name) }
  it { should respond_to(:abbreviation) }

  it { should be_valid } #checks the validation. (doesn't care if no validation)

  describe "when name is not present" do
    before { @cancer_type.name = "" }
    it { should_not be_valid } #the validation needs to exist to pass
  end
  
  describe "when abbreviation is not present" do
    before { @cancer_type.abbreviation = "" }
    it { should_not be_valid }
  end

  describe "when cancer type name or abbreviation is already taken" do
    before do
      #cancer_type_same = @cancer_type.dup
      cancer_type_new = CancerType.new(name: "ThisTumor", abbreviation: "TT")
      cancer_type_new.save
    end

    it { should_not be_valid } #comparing to the object that just got "saved"
  end
end
