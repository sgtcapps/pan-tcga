require 'spec_helper'

describe TargetSet do
    before do
      # needs to be entered correctly or it will fail
      @target_set = TargetSet.new(name: "TargetGenes", cancer_type_id: "3")
    end

  subject { @target_set }

  it { should respond_to(:name) }

  it { should be_valid } 

  describe "when name is not present" do
    before { @target_set.name = "" }
    it { should_not be_valid }
  end

  describe "when targetset format is valid" do
    it "should be valid" do
      targetsets = ['TargetGenes', 'Cancer-miR-500']
      targetsets.each do |valid_targetset|
        @target_set.name = valid_targetset
        should be_valid
      end
    end
  end

  describe "when targetset format is invalid" do
    it "should be invalid" do
      targetsetsets = ['_23andMe', 'Nuclear Assay', 'molecularProtein!']
      targetsets.each do |invalid_targetset|
        @target_set.name = invalid_targetset
        should_not be_valid
      end
    end
  end

  describe "when name is already taken" do
    before do
      target_set_same = @target_set.dup
      #target_set_same.name = 'Cancer-miR"' #causes a fail because is unique
      target_set_same.save
    end

    it { should_not be_valid } #comparing to the object that just got "saved"
  end

end
