unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
    filepath='/mnt/tcgaweb/Current/ClinicalParameters_Genomics_Matrices/*'
    rails_app='/opt/rails/pan-tcga/current/'
    lib_tasks_path="${rails_app}lib/tasks/"
    env='production'

elif [[ "$unamestr" == 'Darwin' ]]; then
    filepath='/Volumes/tcgaweb/Current/ClinicalParameters_Genomics_Matrices/*'
    rails_app='/Users/JenniferPalm/dev/Rails/pan-tcga/'
    lib_tasks_path="${rails_app}lib/tasks/"
    env='development'
fi
split_files_path="${lib_tasks_path}split_files/"
filepath_no_glob=${filepath%\*}
split_files=$split_files_path$csv_file*