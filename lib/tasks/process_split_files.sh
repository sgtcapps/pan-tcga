#!/bin/bash

# process_split_files.sh

csv_file=$1

#for debug
#echo "process_split_files received $csv_file"
#sleep 10

source ./common_vars.sh

rails_app_home () {
  cd $rails_app
}

lib_tasks_home () {
  cd $lib_tasks_path
}

split_files_home () {
  cd $split_files_path
  #echo "split_files_home, cd split_files_path, in dir: `pwd`" 
}

lib_tasks_home

split_big_csv_into_small_chunks () {

  calculate_split_size

  if [ $((count%number_of_split_lines)) = 0 ]; then
    numfiles=$((count/number_of_split_lines))
  else
    numfiles=$((count/number_of_split_lines+1))
  fi

  echo "lines in file: $count"
  echo "splitting $csv_file into $numfiles files with max $number_of_split_lines lines"
  split_files_home && split -l $number_of_split_lines $filepath_no_glob$csv_file $csv_file
}

calculate_split_size () {
  count=$(cat $filepath_no_glob$csv_file | wc -l)
  size_check=1000
  limit=5000
  while [ $size_check -le $limit ]; do
    #echo "top of while, before if, size_check $size_check"
    if [ $count -le $size_check ]; then
      #echo "$count lines is less than/equal $size_check"
      number_of_split_lines=$(($size_check/10))
      #echo "number_of_split_lines: $number_of_split_lines" 
      break    
    elif [ $count -ge $limit ]; then
      echo "$count lines is greater than/equal $limit"
      number_of_split_lines=500
      #echo "number_of_split_lines: $number_of_split_lines" 
    fi
    size_check=$(($size_check+1000)) 
  done   
}

add_csv_header_to_split_files () {
  for sf in $split_files
    do          
      firstline=$(head -n 1 $sf)       
      firstword=$(echo $firstline | cut -d" " -f1)
      if [ $firstword = 'COMMON' ]; then
        continue
      fi
      #echo "firstword: $firstword"
      $(echo sed -n 1p $filepath_no_glob$csv_file) | cat - $sf > "${split_files_path}tempfile" && mv "${split_files_path}tempfile" $sf
    done 
}

send_split_files_to_rake_task () {
  for sf in $split_files
    do
      split_file=`basename $sf`
      echo "Passing split file to rake task: $split_file"
      if [ $env = 'development' ]; then
        #lib_tasks_home && nohup rake RAILS_ENV=$env data_files:load_matrices_table[$split_file] &
        lib_tasks_home && rake RAILS_ENV=$env data_files:load_matrix_table[$split_file] &
          #pid=$!; wait $pid 
        #lib_tasks_home && nuhup rake RAILS_ENV=$env data_files:show_file_name[$sf] &
      elif [ $env = 'production' ]; then
        #lib_tasks_home && rake RAILS_ENV=$env data_files:show_file_name[$sf] &
        lib_tasks_home && rake RAILS_ENV=$env data_files:load_matrix_table[$split_file] &
      fi 
    done
    wait
}

split_big_csv_into_small_chunks
add_csv_header_to_split_files
send_split_files_to_rake_task

echo "process_split_files done with $csv_file"
echo
#echo "Removing split files..."
#split_files_home
#rm *

