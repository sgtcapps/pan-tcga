#!/bin/bash

# run_matrix_load.sh - script to drive rake task for migrating data for "matrix" portion of pan-tcga

usage(){
    echo "Usage: $0 target: <gene|mir|protein> <start position> <offset> <listfiles|run> (view list of files or run scripts)"
    exit 1
}
[[ $# -ne 4 ]] && usage

target=$1
start_pos=$2
offset=$3
to_do=$4

source ./common_vars.sh

lib_tasks_home () {
  cd $lib_tasks_path
}

lib_tasks_home

if [ $target == 'gene' ]; then
    target_name='TargetGenes'
elif [ $target == 'protein' ]; then
    target_name='Allprotein'
elif [ $target == 'mir' ]; then
    target_name='AllmiR'
fi

if [ $to_do == 'run' ]; then
  echo -e "\nRUN MATRIX LOAD BEGINNING..."
elif [ $to_do == 'listfiles' ]; then
  echo -e "\nLISTING FILES..."
fi
echo "target name: $target_name, start position: $start_pos, offset $offset"  
echo

# get list of files designated by arg
if [ $target = 'gene' ]; then # temp skip Methyl*
#  files=($(ls $filepath | grep -vE "ClinicalParameters.txt|\/READ_|\/COAD_|Methyl*" | grep "$target_name"))
  files=($(ls $filepath | grep -vE "ClinicalParameters.txt|\/READ_|\/COAD_" | grep "$target_name"))
else
  files=($(ls $filepath | grep -vE "ClinicalParameters.txt|\/READ_|\/COAD_" | grep "$target_name"))  
fi

i=0
for f in ${files[@]:$start_pos:$offset}
  do
    file=`basename $f`
    echo "Going to process_split_files: $i, $file"
    if [ $to_do == 'run' ]; then ./process_split_files.sh "$file"; fi
    i=$((i+1))
  done # /iterate source files

if [ ! $to_do == 'listfiles' ]; then echo "RUN MATRIX LOAD DONE. Loaded $i files"; fi
#echo "Removing split files..."
#split_files_home
#rm *

# nohup $(rake RAILS_ENV=production data_files:load_matrices_table[]) &
