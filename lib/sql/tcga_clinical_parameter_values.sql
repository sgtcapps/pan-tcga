select cpv.id as 'clin param value id', cp.name as 'clinical paramter', cpv.cancer_type_id as 'cancer type id', ct.abbreviation as 'cancer code', cpv.clinical_parameter_id as 'clin param id', cpv.value as 'clin param value', cpv_ts.target_set_id as 'target id', ts.name as 'target set' 
from clinical_parameter_values cpv
  join clin_par_val_targ_sets cpv_ts on cpv_ts.clinical_parameter_value_id = cpv.id 
  join target_sets ts on ts.id = cpv_ts.target_set_id
  join cancer_types ct on ct.id = cpv.cancer_type_id 
  join clinical_parameters cp on cp.id = cpv.clinical_parameter_id
where cpv.clinical_parameter_id=3 order by cpv.id;
