# from mysql prompt:
# usage: source /path/to/thisfile/tcga_clin_param_gen_val.sql

# each genomic_value record has a genomic. Could be thousands of records with this genomic,
# each one with a target and it's value.

#select
  #s.sample_code as 'sample code',
  #ct.abbreviation as 'cancer_type',
  #gv.genomic_id as 'genomic_value genomic id',
  #gn.name as 'genomic name',
  #count(gn.name) as 'count genomic',
  #gv.target_type, 
  #gv.target_id as 'genomic_value target id',
  #t.name as 'target name', 
  #gv.value as 'genomic_value value'
#from genomic_values as gv 
#join samples as s on s.id=gv.sample_id
#join cancer_types as ct on ct.id=gv.cancer_type_id 
#join mirs as t on t.id=gv.target_id and gv.target_type='Mir'
#join genomics as gn on gn.id=gv.genomic_id
#where gv.genomic_id=11 
#where gv.cancer_type_id=17 and gv.target_id=664
#group by gn.name
#limit 100

select cpv.clinical_parameter_id, cp.name, cpv.sample_id, s.sample_code, cpv.value
from clinical_parameter_values as cpv 
left outer join clinical_parameters as cp on cp.id=cpv.clinical_parameter_id
left outer join samples as s on s.id=cpv.sample_id
where cpv.id=3

