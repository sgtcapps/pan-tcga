create temporary table temp_cpv as (
select 
  clinical_parameter_values.id as 'clin_param_values_id', 
  clinical_parameter_values.cancer_type_id as 'cancer_type_id',
  cancer_types.abbreviation as 'cancer_type',
  clinical_parameter_values.sample_id as 'sample_id',
  samples.sample_code as 'sample_code',
  clinical_parameter_values.clinical_parameter_id as 'clinical_param_id',
  clinical_parameters.name as 'clinical_param_name', 
  clin_par_val_targ_sets.target_set_id as 'target_set_id'
from clinical_parameter_values 
  left outer join clin_par_val_targ_sets on clin_par_val_targ_sets.clinical_parameter_value_id = clinical_parameter_values.id
  join cancer_types on cancer_types.id = clinical_parameter_values.cancer_type_id
  join samples on samples.id = clinical_parameter_values.sample_id
  join clinical_parameters on clinical_parameters.id = clinical_parameter_values.clinical_parameter_id
and clinical_parameter_values.clinical_parameter_id = 3
order by clinical_parameter_values.id);

create temporary table temp_cpv_targets as (
select 
  temp_cpv.clin_param_values_id, 
  temp_cpv.cancer_type_id,
  temp_cpv.cancer_type,
  temp_cpv.sample_id,
  temp_cpv.sample_code,
  temp_cpv.clinical_param_id,
  temp_cpv.clinical_param_name, 
  temp_cpv.target_set_id,
  target_sets.name as 'target_name'
from temp_cpv
left outer join target_sets on target_sets.id = temp_cpv.target_set_id
order by temp_cpv.clin_param_values_id
);

## create csv file (change outfile path to your choice)
select 
  'clin_param_values_id', 
  'cancer_type_id',
  'cancer_type',
  'sample_id',
  'sample_code',
  'clinical_param_id',
  'clinical_param_name', 
  'target_set_id',
  'target_name'
union all
(select 
  clin_param_values_id, 
  cancer_type_id,
  cancer_type,
  sample_id,
  sample_code,
  clinical_param_id,
  clinical_param_name, 
  target_set_id,
  target_name
from temp_cpv_targets
into outfile '/tmp/clinical_parameter_values.csv'
fields terminated by ','
enclosed by '"'
lines terminated by '\n');

